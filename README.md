# Installing locally

* Checkout this repository
* Create a virtualenv: `virtualenv ve`
* Activate the virtualenv: `. ve/bin/activate`
* Install the requirements: `pip install -r requirements.txt`
* Create a `localsettings.py`: `cp localsettings.py.sample localsettings.py`
* Run migrations (creates the DB): `./manage.py migrate`
* Install bower: `npm install`
* Download and build static assets: `node_modules/.bin/grunt`
* Run the webserver: `./manage.py runserver`

# Installing in production
* apt install python memcached npm python-libravatar python-memcached \
  python-tz python-requests python-pil python-psycopg2 virtualenv

# Also see

https://wiki.debconf.org/wiki/Wafer
