$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    if ($('.live-stream').length) {
      videojs('live-stream').videoJsResolutionSwitcher();
    }
});
