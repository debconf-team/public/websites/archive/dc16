from itertools import chain

import django
from django.conf.urls import url
from django.contrib import admin
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse

from dc16.forms import PaymentForm, FrontDeskForm, RoomForm
from dc16.models import PayingAttendee, FrontDeskAttendee


@admin.register(PayingAttendee)
class PayingAttendeeAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__first_name', 'user__last_name',
                     'user__username']
    list_display = ['display_name', 'username', 'email', 'paid',
                    'latest_payment_status']
    fieldsets = [
        ('Attende', {
            'fields': [
                'display_name',
                'email',
                'contact_number',
                'billing_address',
                'notes',
                'is_registered',
            ],
        }),
        ('Board and Lodging', {
            'fields': [
                'arrival',
                'departure',
                'final_dates',
                'nights',
                'accommodation_debcamp',
                'food_debcamp',
                'accommodation_debconf',
                'food_debconf',
                'family_size',
            ]
        }),
        ('Payment', {
            'fields': [
                'paid',
                'total_fee',
                'payments',
            ],
        }),
    ]
    readonly_fields = list(chain(*(
        fieldset[1]['fields'] for fieldset in fieldsets)))
    change_form_template = 'dc16/admin/change_payingattendee.html'

    def save_model(self, request, obj, form, change):
        # Just in case
        return

    def get_actions(self, request):
        actions = super(PayingAttendeeAdmin, self).get_actions(request)
        actions.pop('delete_selected')
        return actions

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return (request.user.is_superuser or
                request.user.groups.filter(
                    name__in=('Registration', 'Accounts')).exists())

    def has_module_permission(self, request):
        return True

    def get_urls(self):
        urls = super(PayingAttendeeAdmin, self).get_urls()
        my_urls = [
            url(r'^(.+)/process_payment/$',
                self.admin_site.admin_view(self.process_payment_view),
                name='dc16_payingattendee_process_payment'),
        ]
        return my_urls + urls

    def process_payment_view(self, request, id_):
        if not self.has_change_permission(request):
            raise PermissionDenied()
        if request.method == 'POST':
            form = PaymentForm(request.POST)
            if form.is_valid():
                person = self.get_object(request, id_)
                person.process_payment(**form.cleaned_data)
                return HttpResponseRedirect(
                    reverse('admin:dc16_payingattendee_change', args=(id_,)))
        else:
            form = PaymentForm()

        kwargs = {}
        if django.VERSION >= (1, 8):
            kwargs['request'] = request
        context = dict(
            self.admin_site.each_context(**kwargs),
            form=form,
        )
        return TemplateResponse(request, 'dc16/admin/crispy_form.html',
                                context)


@admin.register(FrontDeskAttendee)
class FrontDeskAttendeeAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__first_name', 'user__last_name',
                     'user__username']
    list_display = ['display_name', 'username', 'room', 'fee', 't_shirt', 'arrived',
                    'got_tshirt', 'got_tshirt_size', 'got_swag']
    fieldsets = [
        ('Attendee', {
            'fields': [
                'display_name',
                'username',
                'nickname',
                'fee',
            ],
        }),
        ('Front Desk', {
            'fields': [
                'arrived',
                't_shirt',
                'got_tshirt',
                'got_tshirt_size',
                'got_swag',
            ],
        }),
        ('Board and Lodging', {
            'fields': [
                'paid',
                'arrival',
                'departure',
                'final_dates',
                'accommodation_debcamp',
                'food_debcamp',
                'accommodation_debconf',
                'food_debconf',
                'room',
                'family_size',
                'family_usernames',
            ]
        }),
    ]
    readonly_fields = list(chain(*(
        fieldset[1]['fields'] for fieldset in fieldsets)))
    change_form_template = 'dc16/admin/change_frontdeskattendee.html'

    def save_model(self, request, obj, form, change):
        # Just in case
        return

    def get_actions(self, request):
        actions = super(FrontDeskAttendeeAdmin, self).get_actions(request)
        actions.pop('delete_selected')
        return actions

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return (request.user.is_superuser or
                request.user.groups.filter(name='Front Desk').exists())

    def has_module_permission(self, request):
        return True

    def get_urls(self):
        urls = super(FrontDeskAttendeeAdmin, self).get_urls()
        my_urls = [
            url(r'^(.+)/process/$',
                self.admin_site.admin_view(self.process_frontdesk_view),
                name='dc16_frontdeskattendee_process'),
            url(r'^(.+)/room/$',
                self.admin_site.admin_view(self.room_frontdesk_view),
                name='dc16_frontdeskattendee_room'),
        ]
        return my_urls + urls

    actions = [
        'mark_arrived',
        'give_tshirt_and_swag',
    ]

    def mark_arrived(self, request, queryset):
        for attendee in queryset.all():
            attendee.process_fd_form({'arrived': True})
    mark_arrived.short_description = 'Mark as arrived'

    def give_tshirt_and_swag(self, request, queryset):
        for attendee in queryset.all():
            attendee.process_fd_form({
                'arrived': True,
                'got_tshirt': True,
                'got_swag': True,
                'got_tshirt_size': attendee.t_shirt(),
            })
    give_tshirt_and_swag.short_description = 'Give all the things'

    def prepare_form(self, form, person):
        initial = {}
        for field in form().fields:
            try:
                initial[field] = getattr(person, field)()
            except AttributeError as e:
                print e
        return form(initial=initial)

    def process_frontdesk_view(self, request, id_):
        if not self.has_change_permission(request):
            raise PermissionDenied()
        person = self.get_object(request, id_)
        if request.method == 'POST':
            form = FrontDeskForm(request.POST)
            if form.is_valid():
                person.process_fd_form(form.cleaned_data)
                return HttpResponseRedirect(
                    reverse('admin:dc16_frontdeskattendee_change',
                            args=(id_,)))
        else:
            form = self.prepare_form(FrontDeskForm, person)

        kwargs = {}
        if django.VERSION >= (1, 8):
            kwargs['request'] = request
        context = dict(
            self.admin_site.each_context(**kwargs),
            form=form,
        )
        if person.paid() is False:
            context['not_paid'] = True
        return TemplateResponse(request, 'dc16/admin/crispy_form.html',
                                context)

    def room_frontdesk_view(self, request, id_):
        if not (request.user.is_superuser or
                request.user.groups.filter(name='Registration').exists()):
            raise PermissionDenied()
        person = self.get_object(request, id_)
        if request.method == 'POST':
            form = RoomForm(request.POST)
            if form.is_valid():
                person.process_fd_form(form.cleaned_data)
                return HttpResponseRedirect(
                    reverse('admin:dc16_frontdeskattendee_change',
                            args=(id_,)))
        else:
            form = self.prepare_form(RoomForm, person)

        kwargs = {}
        if django.VERSION >= (1, 8):
            kwargs['request'] = request
        context = dict(
            self.admin_site.each_context(**kwargs),
            form=form,
        )
        return TemplateResponse(request, 'dc16/admin/crispy_form.html',
                                context)
