from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import ButtonHolder, HTML, Layout, Submit

from dc16.registration import tshirt_sizes


class PaymentForm(forms.Form):
    status = forms.ChoiceField(
        label='Billing status',
        choices=(
            ('invoiced', 'Invoiced'),
            ('paid', 'Paid'),
            ('refunded', 'Refunded'),
        ))
    amount = forms.DecimalField(
        label='Amount paid',
        decimal_places=2,
        required=False,
    )
    method = forms.ChoiceField(
        label='Payment method',
        choices=(
            (None, 'N/A'),
            ('cc', 'Credit Card'),
            ('eft', 'EFT (Bank Transfer)'),
            ('cash', 'Cash'),
            ('correction', 'Correction of a previous payment'),
        ),
        required=False,
    )
    reference = forms.CharField(
        label='Payment reference',
        required=False,
    )
    notes = forms.CharField(
        label='Payment Notes',
        widget=forms.Textarea,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(PaymentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.add_layout(Layout(
            'status',
            'amount',
            'method',
            'reference',
            'notes',
            ButtonHolder(
                Submit('Save', 'Save', css_class='button white'),
                HTML('<a class="btn btn-warning" '
                     'href="javascript:window.history.back()">'
                     'Cancel</a>'),
            ),
        ))

    def clean(self):
        cleaned_data = super(PaymentForm, self).clean()
        if cleaned_data['status'] in ('paid', 'refunded'):
            for field in ('amount', 'method'):
                if not cleaned_data.get(field):
                    self.add_error(field, 'Required for payments')
        if (cleaned_data['status'] == 'refunded' and
                cleaned_data.get('amount') > 0):
            self.add_error('amount', 'Use negative values for refunds')


class RoomForm(forms.Form):
    room = forms.CharField(
        label='Room number',
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(RoomForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.add_layout(Layout(
            'room',
            ButtonHolder(
                Submit('Save', 'Save', css_class='button white'),
                HTML('<a class="btn btn-warning" '
                     'href="javascript:window.history.back()">'
                     'Cancel</a>'),
            ),
        ))


class FrontDeskForm(forms.Form):
    arrived = forms.BooleanField(
        label='Arrived?',
        required=False,
    )

    got_tshirt = forms.BooleanField(
        label='Got their t-shirt?',
        required=False)

    got_tshirt_size = forms.ChoiceField(
        label='Size of t-shirt they got',
        choices=tshirt_sizes(),
        required=False)

    got_swag = forms.BooleanField(
        label='Got their swag?',
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(FrontDeskForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.add_layout(Layout(
            'arrived',
            'got_tshirt',
            'got_tshirt_size',
            'got_swag',
            ButtonHolder(
                Submit('Save', 'Save', css_class='button white'),
                HTML('<a class="btn btn-warning" '
                     'href="javascript:window.history.back()">'
                     'Cancel</a>'),
            ),
        ))
