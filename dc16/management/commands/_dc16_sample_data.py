# -*- coding: utf-8 -*-
#
# Sample data for DC16/wafer
#
# © 2015 by martin f. krafft <madduck@debconf.org>
# WTFPL
#

######## SITES ##########

SITE = dict(domain='debconf16.debconf.org', name='DebConf16')

######## USERS ##########

USERS = (('madduck','Martin','Krafft','madduck@debconf.org',True),
         ('tumbleweed','Stefano','Rivera','tumbleweed@debian.org',True),
         ('cate','Giacomo','Catenazzi','cate@debian.org',True),
         ('dlange','Daniel','Lange','DLange@debconf.org',True),
         ('john','John','Doe','john@example.org',False),
         ('jane','Jane','Doe','jane@example.org',False)
         )

####### TICKETS #########

TICKET_TYPES = ('Standard attendee',
                'Supporting attendee',
                'Professional attendee',
                'Corporate attendee')
import itertools
TICKETS = [(hash(t+str(n))%100000, t[:3]+str(n), t)
        for t,n in itertools.product(TICKET_TYPES, (1,2,3,4))]

####### PAGES ##########

INDEX_PAGE = u"""# About DebConf

DebConf is the annual [Debian](http://debian.org) developers meeting, an event
filled with discussions, workshops and coding parties – all of them highly
technical in nature. DebConf16, the 17th Debian Conference, will take place
from 3 to 9 July 2016 at the [University of Cape Town](http://www.uct.ac.za/),
[Cape Town](https://en.wikipedia.org/wiki/Cape_Town), [South
Africa](https://en.wikipedia.org/wiki/South_Africa).

The schedule of events will again feature speakers from around the world. The
conference is expected to be extremely beneficial for developing key
components of the Debian system, infrastructure and community.
"""

####### TALKS ##########

TALK_TYPES = (('Plenary', 'Exclusive presentations geared at all conference attendees'),
              ('Presentation', 'Traditional talks, presenting to an audience'),
              ('Workshop', 'Hands-on tutorials'),
              ('Panel discussion', 'On-stage discussion rounds'),
              ('Social event', 'Social events of all kinds'),
              ('BoF', 'Birds-of-a-feather sessions'),
              ('Sprint', 'Topical development sessions'),
              ('Other', 'Events that do not fit other categories'),
              )

TALKS = (('Opening', 'Plenary', 'Opening ceremony', 'A', 'tumbleweed', ('dlange', 'john')),
         ('Closing', 'Plenary', 'Closing ceremony', 'A', 'tumbleweed', ('cate', 'madduck')),
         ('DC17', 'Presentation', 'DC17 in X', 'P', 'john', ('jane',)),
         ('DC18', 'BoF', 'DC18 ideas', 'R', 'madduck', ()),
         ('C&W party', 'Social event', 'Much cheese, much wine', 'P', 'dlange', ()),
         )

####### SCHEDULE #######

from datetime import date, datetime, timedelta
DAYS = [date(2016,7,2) + timedelta(d) for d in xrange(0,8)]

VENUES = ((1,'Theatre',DAYS),
          (3,'CompSci lab',DAYS[4:]),
          (2,'Auditorium',DAYS[:-2]))

_SLOTLENMINS = 60
_DAYSTARTHOUR=9
_DAYENDHOUR=18
SLOTS = dict([(d,[]) for d in DAYS])
for k,v in SLOTS.iteritems():
    t = datetime(k.year,k.month,k.day,_DAYSTARTHOUR,0,0)
    nrslots = (_DAYENDHOUR - _DAYSTARTHOUR) * 60/_SLOTLENMINS + 1
    slotdelta = timedelta(0,0,0,0,_SLOTLENMINS,0)

    def make_name_slot_pair(n):
        start = t + n*slotdelta
        name = start.strftime('%Y-%m-%d, slot {}'.format(n))
        return (name, start)
    v.extend(map(make_name_slot_pair, xrange(0, nrslots)))

TALK_SCHEDULE_ITEMS = ((TALKS[0], VENUES[0], SLOTS[DAYS[0]][0:2]),
                       (TALKS[1], VENUES[0], SLOTS[DAYS[7]][8:9]),
                       )
