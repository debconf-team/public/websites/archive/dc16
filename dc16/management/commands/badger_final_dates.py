from optparse import make_option

from django.contrib.auth.models import Group
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime
from django.utils.formats import date_format

from wafer.users.models import UserProfile


subject = 'DebConf - final travel dates required!'
txt = '''Dear attendee,

DebConf is now just under four weeks away, and DebCamp starts in just over two
weeks. Since we need to get final numbers of attendees to our suppliers, we
must make absolutely certain when exactly you will be attending.

Please mark your dates as final in Registration on
https://debconf16.debconf.org/accounts/profile/ by tomorrow: Wednesday, the 8th
of June 2016.

We can't wait any longer as there is quite some work involved in allocating 200
rooms.  In addition, we must also finalise any information about food
preferences and allergies now.

If you have not marked your dates as final by the deadline, we will take the
dates you have provided regardless, to plan for your accommodation and invoice
you (where relevant).

Please note that paying on-site will incur a 20%% service charge, so it is in
your best interest to fix the dates of your attendance as quickly as possible,
by Wednesday, the 8th of June 2016.

This is the data we have for you on file:

Arriving on %(arrival)s
Departing on %(departure)s
%(roomboard)s

If any of the above is inaccurate, please fix the data as you mark your
attendance dates final.

Best regards,
The DebConf Registration Team
'''


class Command(BaseCommand):
    help = "Badger users who don't have final dates"

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, user_profile, dry_run):
        if dry_run:
            print('I would badger: %s'
                  % user_profile.display_name().encode('utf-8'))
            return
        to = user_profile.user.email

        def reg(key):
            return user_profile.kv.get(group=self.reg_group, key=key).value

        arrival = parse_datetime(reg('arrival'))
        departure = parse_datetime(reg('departure'))
        roomboard = []

        for event in ('DebCamp', 'DebConf'):
            for service in ('food', 'accommodation'):
                if reg('%s_%s' % (service, event.lower())):
                    roomboard.append('Requiring %s for %s' % (service, event))

        body = txt % {
            'arrival': date_format(arrival, 'DATETIME_FORMAT'),
            'departure': date_format(departure, 'DATETIME_FORMAT'),
            'roomboard': '\n'.join(roomboard),
        }
        email_message = EmailMultiAlternatives(subject, body, to=[to])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options.get('yes')
        self.reg_group = Group.objects.get_by_natural_key('Registration')

        if dry_run:
            print "Not actually doing anything without --yes"

        for user_profile in UserProfile.objects.all():
            if not user_profile.is_registered():
                continue

            final_dates = user_profile.kv.filter(
                group=self.reg_group, key='final_dates').first()
            if final_dates:
                final_dates = final_dates.value

            if not final_dates:
                self.badger(user_profile, dry_run)
