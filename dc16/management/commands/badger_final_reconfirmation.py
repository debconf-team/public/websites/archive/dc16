from optparse import make_option

from django.contrib.auth.models import Group
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime
from django.utils.formats import date_format

from wafer.users.models import UserProfile


subject = 'DebConf16 - reconfirm NOW - last chance! 12:00 UTC Sunday 22 May'
txt = '''
Dear prospective DebConf attendee,

The reconfirmation deadline for DebConf16 has now passed. Since you have not
yet reconfirmed and we have no further information from you, we must assume
that you no longer plan to attend DebConf. This makes us sad.

If you still plan on attending the conference, even if it depends on a bursary
decision, please notify us immediately by writing to registration@debconf.org.
Also click reconfirm, and check your registration data (especially arrival date
& time) at https://debconf16.debconf.org

Alternatively, if you have decided not to attend, please unregister from the
conference. We will automatically unregister you, anyway. But you can save us
the effort of trying to talk to you, first. If you unregister, any outstanding
invoices will be cancelled.

To unregister, update your registration, unchecking DebConf, DebCamp,
accommodation and food. Check that the form is accepted and you get to a
success screen. If you are having difficulty unregistering, please contact the
registration team by replying to this mail.

If you are still undecided, please contact us at once with any questions or
concerns.

Best wishes,
The inconsolable DebConf Registration Team
'''


class Command(BaseCommand):
    help = "Badger users who haven't reconfirmed (last call)"

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, user_profile, dry_run):
        if dry_run:
            print('I would badger: %s'
                  % user_profile.display_name().encode('utf-8'))
            return
        to = user_profile.user.email

        email_message = EmailMultiAlternatives(subject, txt, to=[to])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options.get('yes')
        self.reg_group = Group.objects.get_by_natural_key('Registration')

        if dry_run:
            print "Not actually doing anything without --yes"

        for user_profile in UserProfile.objects.all():
            if not user_profile.is_registered():
                continue

            reconfirm = user_profile.kv.filter(
                group=self.reg_group, key='reconfirm').first()
            if reconfirm:
                reconfirm = reconfirm.value

            if not reconfirm:
                self.badger(user_profile, dry_run)
