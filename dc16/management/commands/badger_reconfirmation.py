from optparse import make_option

from django.contrib.auth.models import Group
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime
from django.utils.formats import date_format

from wafer.users.models import UserProfile


subject = 'DebConf16 reconfirmation required'
txt = '''Dear DebConf attendee,

As DebConf16 draws ever nearer, it is time for the team to make sure who is
actually attending this year.  To this end, we have just opened the
reconfirmation phase.

To reconfirm, simply visit your registration page at
https://debconf16.debconf.org/accounts/profile/ tick the reconfirmation button
(located just above "about me" on the registration form), and hit "save".

Please do so by Friday, May 20th.  After this, we cannot guarantee to
accommodate you at the conference.

Please make sure at this time that you have selected your dates correctly, and
tick "My dates are final".  The time part of those dates matter for
accommodation and food planning.

If it turns out you can no longer make it to DebConf, please uncheck "I plan to
attend DebCamp/DebConf", and set the attendance type to "basic attendee".

We have you registered as:
Arriving on %(arrival)s
Departing on %(departure)s
%(roomboard)s

Best wishes,
The DebConf Registration Team
'''


class Command(BaseCommand):
    help = "Badger users who haven't reconfirmed"

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, user_profile, dry_run):
        if dry_run:
            print('I would badger: %s'
                  % user_profile.display_name().encode('utf-8'))
            return
        to = user_profile.user.email

        def reg(key):
            return user_profile.kv.get(group=self.reg_group, key=key).value

        arrival = parse_datetime(reg('arrival'))
        departure = parse_datetime(reg('departure'))
        roomboard = []

        for event in ('DebCamp', 'DebConf'):
            for service in ('food', 'accommodation'):
                if reg('%s_%s' % (service, event.lower())):
                    roomboard.append('Requiring %s for %s' % (service, event))

        body = txt % {
            'arrival': date_format(arrival, 'DATETIME_FORMAT'),
            'departure': date_format(departure, 'DATETIME_FORMAT'),
            'roomboard': '\n'.join(roomboard),
        }
        email_message = EmailMultiAlternatives(subject, body, to=[to])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options.get('yes')
        self.reg_group = Group.objects.get_by_natural_key('Registration')

        if dry_run:
            print "Not actually doing anything without --yes"

        for user_profile in UserProfile.objects.all():
            if not user_profile.is_registered():
                continue

            reconfirm = user_profile.kv.filter(
                group=self.reg_group, key='reconfirm').first()
            if reconfirm:
                reconfirm = reconfirm.value

            if not reconfirm:
                self.badger(user_profile, dry_run)
