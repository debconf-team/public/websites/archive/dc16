from optparse import make_option

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from wafer.users.models import UserProfile


subject = 'DebConf 16 registration - missing name'
txt = '''Hello,

Thank you for registering for DebConf 16 in Cape Town. We've noticed that
you've not provided a name during your registration.

Though we absolutely respect your right to privacy, from a practical
perspective we find it necessary to ask you to provide a name.  It doesn't have
to be your legal name, but it's what's going to appear on DebConf-related
paperwork. We assume you can come up with something more imaginative than
"Unknown User".

That way, we can ensure that (if self-paying) you are billed correctly, and
that (in any case) we know what needs to go on your attendee badge. Please
visit your registration page on http://debconf16.debconf.org/accounts/profile/,
click "Edit User", and fill in your name there.

If you have any questions or concerns, please do not hesitate to contact the
Registration Team at registration@debconf.org.

Best regards,
the DebConf 16 Registration Team
'''


class Command(BaseCommand):
    help = "Badger users who haven't provided a name."

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, user_profile, dry_run):
        if dry_run:
            print('I would badger: %s'
                  % user_profile.display_name().encode('utf-8'))
            return
        to = user_profile.user.email
        email_message = EmailMultiAlternatives(subject, txt, to=[to])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options.get('yes')
        if dry_run:
            print "Not actually doing anything without --yes"
        for user_profile in UserProfile.objects.all():
            if not user_profile.is_registered():
                continue
            user = user_profile.user
            if user.first_name == u'Unknown User' or (
                    not user.first_name and not user.last_name):
                self.badger(user_profile, dry_run)
