from optparse import make_option

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from wafer.users.models import UserProfile


subject = 'DebConf16: Registration Reminder'
txt = '''Hi,

We see that you have created an account on https://debconf16.debconf.org/
But haven't actually registered to attend the conference, yet.

If you are intending to attend, you need to go to:
<https://debconf16.debconf.org/accounts/profile/>

Click Register, and fill out the form.

When you are successfully registered, you'll receive a confirmation e-mail, and
see a big green "Registered" label on your profile.

We recommend that you register as soon as possible, if you want to be
accommodated in the university residence, on site.  If you need to amend your
registration later, you can.

Unfortunately, bursary applications have already closed.  But diversity
bursaries are still available. (In fact, they haven't even opened yet. We
expect to be announcing them in a few days.)

If you don't intend to attend the conference, don't worry. We won't badger you
any more.

Hope to see you in Cape Town,

The DebConf16 team
'''


class Command(BaseCommand):
    help = "Badger users who haven't registered for attendance"

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, user, dry_run):
        if dry_run:
            print "I would badger: %s" % user.user.email
            return
        to = user.user.email
        email_message = EmailMultiAlternatives(subject, txt, to=[to])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options.get('yes')
        if dry_run:
            print "Not actually doing anything without --yes"
        for user in UserProfile.objects.all():
            if not user.is_registered():
                self.badger(user, dry_run)
