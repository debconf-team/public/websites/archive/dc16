# -*- coding: utf-8 -*-
from optparse import make_option

from django.contrib.auth.models import Group
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime
from django.utils.formats import date_format

from wafer.users.models import UserProfile
from dc16.models.accounts import DEBCONF_END


subject = 'Welcome to DebConf16! (arrival details)'
txt = u'''
Dear %(username)s,

Welcome to DebConf 16, hosted at the University of Cape Town.  This is the
first DebConf on the African continent, we hope that you will enjoy it!

Please read this email carefully.


== The accommodation address ==

Fuller House
Residence Road
UCT Upper Campus
Rondebosch
Cape Town, South Africa

and the venue address (starting Monday, 27 June):

Menzies Building, 3rd Floor
Engineering Mall
UCT Upper Campus
Rondebosch
Cape Town, South Africa


== Before departing for Cape Town ==

Print this email, and the attached PDF which also includes a venue map.

In case of any problems (including delays or cancellation), please contact
registration@debconf.org as soon as possible, or if you are already travelling,
phone or SMS the front-desk at +27 81 303 7458 (use this number sparingly
after-hours South African time).

Check your data (and report any errors and changes as soon as possible to
registration@debconf.org):

Arriving on %(arrival)s
Departing on %(departure)s
Your diet is %(diet)s
%(roomboard)s%(late_departure_warning)s

If you have any special needs, please make sure that registration knows about
it, by sending a mail to registration@debconf.org.

Familiarize yourself with the DebConf CoC [0] and Debian CoC [1].  If you are
harassed, you can contact antiharassment@debian.org.  For other doubts and
problems, the Front Desk and Organisation team may be of help.

[0]: http://debconf.org/codeofconduct.shtml
[1]: https://www.debian.org/code_of_conduct

Note that animal products can't be legally imported into South Africa without
advance approval, which may be relevant to your (optional) contribution to the
Cheese and Wine party.

Check your travel documents.  (Will your passport remain valid for 30 days
after your departure from South Africa? Do you have 2 blank pages in it?) Do
you have any necessary visas? [2]

[2]: http://www.home-affairs.gov.za/index.php/countries-exempt-from-sa-visas

Note: have also a printed receipt of return ticket.  Print a receipt for
travel, in the case of sponsored travel.

Warn your bank that you'll be travelling to South Africa, so they don't block
your cards.

Bring a towel (or two, one small for hands and one for body), soap, and other
personal hygiene items.


== What to Bring ==

Bed sheets, a duvet and blanket are provided.

Power strips and international adapters.  The traditional type-M South African
socket [3] isn't compatible with any foreign plugs.  The most recent sockets
are almost identical to the Swiss socket and accept type-C europlugs.  There
are some of these in the Snape hack labs.  Adaptors from the big 3-pin plug to
2-pin europlug sockets are ubiquitous.  We will have some for sale at the front
desk.

[3]: http://en.wikipedia.org/wiki/AC_power_plugs_and_sockets#BS_546_and_related_types

Warm clothes.  It is winter in Cape Town, expect night low temperatures between
4 and 10°C (40-50°F), and daily maximum between 10 and 20°C (50-70°C).  You
will see probably also some heavy showers, so bring a raincoat (umbrellas
aren't well suited to Cape Town winds).  A hot water bottle may come in handy,
too.

An Ethernet cable is always useful to have, if we have WiFi trouble.  WiFi
should be available in every area we use on the campus.

Printed PGP/GPG fingerprint slips, if you want to have you key signed by other
peers.


== Airport Arrival ==

So you've just arrived in Cape Town.  What next?

First, look around you: are there any other likely-looking types who look like
they might be going to DebConf?  Go on, approach them.  They probably won't
bite.  It'll make your taxi fare cheaper.  (Really, you should have coordinated
this on the wiki [4] already.  But anyway.) You can wear a Debian shirt, to
make it easier for other attendees to find you, too.  A good place to meet your
fellow-travellers is the food court in the central area.  There is a coffee
shop, and tables with power.  You may want to buy a local SIM card and power
adaptor from the Woolworths while you wait.

[4] https://wiki.debconf.org/wiki/DebConf16/TravelCoordination/Arrival

There is 30 mins of Free WiFi available at the airport [5] (with email / SMS
verification)

[5]: http://www.saairports.com/free-wifi

ATMs are located in the corridor from the arrivals area to the centre of the
airport.  There are also ATMs at UCT. Any banks should work to withdraw local
currency.
Here are some approximate conversions:
USD 100 -> ZAR 1500
EUR 100 -> ZAR 1700
GBP 100 -> ZAR 2100

Once you are ready to make your way to campus, proceed out to the taxi rank
which is located directly in front of the airport terminal (behind the MyCiTi
bus terminal).  It will generally cost about R200 to get to UCT.  Ask for UCT
Upper Campus as your destination.  Then get them to drop you at Fuller Hall, at
the base of the Jammie steps, on Residence Road.  Fuller Hall is the building
to your right, facing the city.  The city's new bus system is great, but
doesn't extend to UCT, yet.

If you're being accommodated on campus, you'll want to drop your bags off at
the Fuller residence, and then check in to the Front Desk (which they can
direct you to).  You can drive directly to Fuller, although you may have to
park somewhere else, for the rest of the conference.


== Front Desk Location ==

If you arrive at the venue before 6pm, on or after Monday 27 June, it will be
in Menzies 11B.  This is on the 3rd floor of the Menzies building (ground floor
from the mountain side).

If you arrive late (and front desk is closed), you should go straight to
Fuller, and you can register with the front desk the next morning.

The Front desk will provide you a badge, useful information about venue, WiFi
code, meal times, etc.


== Driving directions from the airport ==

Follow the signs to Cape Town, taking the N2.  As you reach the mountain, take
exit 6 for the M3 towards Muizenberg.  Stay in the left lane, and take the
second exit (7) for Woolsack Drive.  Turn right at the traffic lights, and stay
in the left lane.  Take the left hand fork (Rugby Rd), when the road splits in
3 at the Visitors Centre.  Take the hairpin bend to the right, onto Residence
Road.  Fuller Hall is the building to your right.  The entrance is at the
parking lot in the middle.

If you want to park at UCT, you'll need to get a parking permit from the UCT
Visitors Centre (located at the X on the map).  They will charge you ZAR 70 per
week.  We will be required to park in an assigned parking area.

Have a safe trip, we look forward to seeing you in Cape Town,

The DebConf team
'''


class Command(BaseCommand):
    help = "Welcome people to DebConf"

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, user_profile, dry_run):
        if dry_run:
            print('I would badger: %s'
                  % user_profile.display_name().encode('utf-8'))
            return
        to = user_profile.user.email

        def reg(key):
            return user_profile.kv.get(group=self.reg_group, key=key).value

        arrival = parse_datetime(reg('arrival'))
        departure = parse_datetime(reg('departure'))
        roomboard = []

        for event in ('DebCamp', 'DebConf'):
            for service in ('food', 'accommodation'):
                if reg('%s_%s' % (service, event.lower())):
                    roomboard.append('Requiring %s for %s' % (service, event))

        late_departure_warning = ''
        if roomboard and departure > DEBCONF_END:
            late_departure_warning = (
                '\nWARNING: No accommodation on site after ' +
                date_format(DEBCONF_END, 'DATETIME_FORMAT'))
        diet = reg('diet') or 'omnivore'

        body = txt % {
            'username': user_profile.user.username,
            'arrival': date_format(arrival, 'DATETIME_FORMAT'),
            'departure': date_format(departure, 'DATETIME_FORMAT'),
            'roomboard': '\n'.join(roomboard),
            'late_departure_warning': late_departure_warning,
            'diet': diet,
        }
        pdf = ('DebConf16-arrival.pdf', self.attachment, 'application/pdf')
        email_message = EmailMultiAlternatives(
            subject, body, to=[to], attachments=[pdf])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options.get('yes')
        self.reg_group = Group.objects.get_by_natural_key('Registration')
        with open('assets/docs/welcome-doc.pdf', 'rb') as f:
            self.attachment = f.read()

        if dry_run:
            print "Not actually doing anything without --yes"
            # Send some test-emails
            #for username in ('stefanor', 'highvoltage', 'nattie', 'cate'):
            #    user_profile = UserProfile.objects.get(user__username=username)
            #    self.badger(user_profile, False)

        for user_profile in UserProfile.objects.all():
            if not user_profile.is_registered():
                continue

            self.badger(user_profile, dry_run)
