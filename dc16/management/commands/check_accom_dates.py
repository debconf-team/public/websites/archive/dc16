from optparse import make_option

from datetime import timedelta

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group
from django.utils.dateparse import parse_datetime

from wafer.users.models import UserProfile

from dc16.models.accounts import (DEBCAMP_START, DEBCONF_START, DEBCONF_END,
                                  count_nights)

subject = 'DebConf16: Registration Details'
txt = '''Hi,

Nattie will write an e-mail that goes here.
'''


class Command(BaseCommand):
    help = ('Badger users who are arriving/departing outside the days we '
            'provide accommodation')

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, user, dry_run, accommodation_debcamp,
               accommodation_debconf, arrival, earliest_arrival, departure,
               latest_departure):
        if dry_run:
            print 'I would badger: %s' % user.display_name().encode('utf-8')
            print '%s <-> %s' % (arrival, departure)
            print '%s <-> %s' % (earliest_arrival, latest_departure)
            return
        to = user.user.email
        email_message = EmailMultiAlternatives(subject, txt, to=[to])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options.get('yes')
        if dry_run:
            print 'Not actually doing anything without --yes'

        reg_group = Group.objects.get_by_natural_key('Registration')

        for user in UserProfile.objects.all():
            if not user.is_registered():
                continue

            def reg(key):
                return user.kv.get(group=reg_group, key=key).value

            accommodation_debcamp = reg('accommodation_debcamp')
            accommodation_debconf = reg('accommodation_debconf')

            if not (accommodation_debcamp or accommodation_debconf):
                # Staying off-campus
                continue

            arrival = parse_datetime(reg('arrival'))
            departure = parse_datetime(reg('departure'))

            earliest_arrival = DEBCONF_END
            latest_departure = DEBCAMP_START
            if accommodation_debcamp:
                earliest_arrival = min(earliest_arrival, DEBCAMP_START)
                latest_departure = max(latest_departure, DEBCONF_START)
            if accommodation_debconf:
                # Grant an extra arrival day, because of the Open Weekend
                earliest_arrival = min(earliest_arrival,
                    DEBCONF_START - timedelta(days=1))
                latest_departure = max(latest_departure, DEBCONF_END)

            if (count_nights(arrival, departure) >
                    count_nights(earliest_arrival, latest_departure)):
                self.badger(user, dry_run, accommodation_debcamp,
                            accommodation_debconf, arrival, earliest_arrival,
                            departure, latest_departure)
