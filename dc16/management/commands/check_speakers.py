from optparse import make_option

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

from wafer.talks.models import Talk

from dc16.models.front_desk import fd_group


class Command(BaseCommand):
    help = "List speakers that have not arrived yet"

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, talk, speaker):
        try:
            scheduleitem = talk.scheduleitem_set.get()
        except ObjectDoesNotExist:
            return

        arrived = speaker.kv.filter(
            group=self.fd_group,
            key='arrived').first()
        if arrived and arrived.value:
            return

        print '%s - %s' % (talk.title, speaker.display_name())


    def handle(self, *args, **options):
        self.fd_group = fd_group()
        for talk in Talk.objects.all():
            for speaker in talk.authors.all():
                self.badger(talk, speaker.userprofile)
