from optparse import make_option

from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from wafer.talks.models import Talk


FROM = 'content@debconf.org'
SUBJECT = 'DebConf - talk scheduled: %(title)s'
BODY = '''Dear speaker / BoF convenor,

Your Talk/BoF titled %(title)s has been scheduled at %(time)s in %(venue)s.

You should be able to see it on: https://debconf16.debconf.org/schedule/

Does that suit you? If not, please contact content@debconf.org

Best regards,
The DebConf Content Team
'''


class Command(BaseCommand):
    help = "Notify speakers that their talks have been scheduled"

    option_list = BaseCommand.option_list + (
        make_option('--yes', action='store_true', default=False,
                    help='Actually do something'),
    )

    def badger(self, talk, dry_run):
        try:
            scheduleitem = talk.scheduleitem_set.get()
        except ObjectDoesNotExist:
            return

        kv, created = talk.kv.get_or_create(
            group=self.content_group,
            key='notified_speaker',
            defaults={'value': None},
        )

        rebadger_key = [scheduleitem.venue.id, scheduleitem.slots.first().id]
        if kv.value == rebadger_key:
            return

        to = [user.email for user in talk.authors.all()]

        subst = {
            'title': talk.title,
            'time': scheduleitem.get_start_time(),
            'venue': scheduleitem.venue.name,
        }

        subject = SUBJECT % subst
        body = BODY % subst

        if dry_run:
            print('I would badger speakers of: %s'
                  % talk.title.encode('utf-8'))
            return
        email_message = EmailMultiAlternatives(
            subject, body, from_email=FROM, to=to)
        email_message.send()

        kv.value = rebadger_key
        kv.save()

    def handle(self, *args, **options):
        dry_run = not options.get('yes')
        self.content_group = Group.objects.get_by_natural_key('Talk Mentors')

        if dry_run:
            print "Not actually doing anything without --yes"

        for talk in Talk.objects.all():
            self.badger(talk, dry_run)
