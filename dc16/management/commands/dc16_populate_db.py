# -*- coding: utf-8 -*-
#
# Populate the wafer database with DC16 data
#
# © 2015 by martin f. krafft <madduck@debconf.org>
# WTFPL
#

import logging

from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = "Populate the database with DebConf standards"

    def handle(self, *args, **options):
        from _dc16_sample_data import SITE
        self._create_dc_site(**SITE)

        from _dc16_sample_data import USERS
        self._create_users(USERS)

        from _dc16_sample_data import INDEX_PAGE
        self._create_page('index', name='DebConf16', content=INDEX_PAGE)

        from _dc16_sample_data import TICKET_TYPES, TICKETS
        self._create_ticket_types(TICKET_TYPES)
        self._create_sample_tickets(TICKETS)

        from _dc16_sample_data import TALK_TYPES, TALKS
        self._create_talk_types(TALK_TYPES)
        self._create_sample_talks(TALKS)

        from _dc16_sample_data import DAYS, VENUES, SLOTS, TALK_SCHEDULE_ITEMS
        self._create_days(DAYS)
        self._create_sample_venues(VENUES)
        self._create_slots(SLOTS)
        self._create_talk_schedule_items(TALK_SCHEDULE_ITEMS)

    def __init__(self, loglevel=logging.INFO):
        super(Command, self).__init__()
        fmt = '%(levelname)s %(message)s'
        logging.basicConfig(level=loglevel, format=fmt)

    @staticmethod
    def _get_repr(klass, logexclude=[], **args):
        return '%s(%s)' % (klass.__name__,
                ', '.join(["%s='%s'" % (k,v) for k,v in args.iteritems() if k not in logexclude]))

    def _check_exists(self, klass, nolog=False, logexclude=[], **filt):
        if klass.objects.filter(**filt).exists():
            instance = klass.objects.filter(**filt)[0]
            if not nolog:
                logging.info('%s already exists, skipping…',
                        Command._get_repr(klass, logexclude, **filt))
            return instance
        return None

    def _create_instance(self, klass, nolog=False, logexclude=[], **data):
        instance = klass(**data)
        instance.save()
        if not nolog:
            logging.info('Wrote %s to database.',
                    Command._get_repr(klass, logexclude, **data))
        return instance

    def _create_dc_site(self, domain, name):
        from django.contrib.sites.models import Site
        if not self._check_exists(Site, domain=domain, name=name):
            self._create_instance(Site, domain=domain, name=name)

    def _create_users(self, userlist):
        from django.contrib.auth.models import User
        for username, fname, lname, email, staff in userlist:
            if not self._check_exists(User, username=username):
                self._create_instance(User, username=username,
                        first_name=fname, last_name=lname, email=email,
                        is_staff=staff)

    def _create_page(self, slug, **kwargs):
        from wafer.pages.models import Page
        if not self._check_exists(Page, slug=slug, logexclude='content'):
            self._create_instance(Page, slug=slug, logexclude='content', **kwargs)

    def _create_ticket_types(self, ttypes):
        from wafer.tickets.models import TicketType
        for ttype in ttypes:
            if not self._check_exists(TicketType, name=ttype):
                self._create_instance(TicketType, name=ttype)

    def _create_sample_tickets(self, tix):
        from wafer.tickets.models import Ticket, TicketType
        for c, e, t in tix:
            if not self._check_exists(Ticket, barcode=c):
                ttype = TicketType.objects.get(name=t)
                self._create_instance(Ticket,
                        barcode=c, type=ttype,
                        email='%s@example.org'%e.lower())

    def _create_talk_types(self, ttypes):
        from wafer.talks.models import TalkType
        for name, description in ttypes:
            if not self._check_exists(TalkType, name=name):
                self._create_instance(TalkType, name=name,
                        description=description)

    def _create_sample_talks(self, talks):
        from wafer.talks.models import Talk, TalkType
        from django.contrib.auth.models import User
        for title, typ, abstract, status, author, people in talks:
            if not self._check_exists(Talk, logexclude=('abstract',), title=title):
                ttype = TalkType.objects.get(name=typ)
                auth = User.objects.get(username=author)
                instance = self._create_instance(Talk,
                        logexclude=('abstract',), title=title,
                        talk_type=ttype, abstract=abstract,
                        status=status,
                        corresponding_author=auth)
                instance.authors.add(*User.objects.filter(username__in=people))
                logging.info("  associated %s with talk '%s'", people,
                        instance.title)

    def _create_days(self, days):
        from wafer.schedule.models import Day
        for d in days:
            if not self._check_exists(Day, date=d):
                self._create_instance(Day, date=d)

    def _create_sample_venues(self, venues):
        from wafer.schedule.models import Venue, Day
        for order, name, days in venues:
            if not self._check_exists(Venue, name=name):
                venue = self._create_instance(Venue, order=order, name=name)
                venue.days.add(*Day.objects.filter(date__in=days))
                logging.info("  made venue %s available on %s", name,
                        map(lambda d: d.isoformat(), days))

    def _create_slots(self, slots):
        from wafer.schedule.models import Day, Slot
        for day, slots in slots.iteritems():
            count = 0
            for i in xrange(0, len(slots)-1):
                name = slots[i][0]
                start = slots[i][1]
                end = slots[i+1][1]
                if not self._check_exists(Slot, nolog=True, name=name):
                    d = Day.objects.get(date=day)
                    self._create_instance(Slot, nolog=True, name=name, day=d,
                            start_time=start, end_time=end)
                    count += 1
            if count > 0:
                logging.info('Wrote %d Slots on %s to database.', count,
                        day.isoformat())
            else:
                logging.info('Slots on %s already exist, skipping…',
                        day.isoformat())

    def _create_talk_schedule_items(self, items):
        from wafer.schedule.models import Slot, Venue, ScheduleItem
        from wafer.talks.models import Talk
        for talk, venue, slots in items:
            talkobj = Talk.objects.get(title=talk[0])
            if not self._check_exists(ScheduleItem, talk=talkobj):
                venueobj = Venue.objects.get(name=venue[1])
                instance = self._create_instance(ScheduleItem, talk=talkobj,
                        venue=venueobj)
                slotnames = [x[0] for x in slots]
                slotobjs = Slot.objects.filter(name__in=slotnames)
                instance.slots.add(*slotobjs)
                logging.info('Added Slots %s to ScheduleItem for "%s".',
                        slotnames, talk[0])
