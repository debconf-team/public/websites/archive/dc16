import codecs
import sys

from django.core.management.base import BaseCommand

if sys.version_info >= (3,):
    import csv
else:
    from backports import csv

from dc16.models.accounts import PayingAttendee


class Command(BaseCommand):
    help = 'Dump bills, for debugging'

    def handle(self, *args, **options):
        stream_writer = codecs.getwriter('utf-8')
        bytestream = getattr(sys.stdout, 'buffer', sys.stdout)
        csv_file = csv.writer(stream_writer(bytestream))
        csv_file.writerow(('username', 'bill', 'details'))
        for user in PayingAttendee.objects.all():
            bill, description = user._bill()
            csv_file.writerow((user.user.username, bill, description))
