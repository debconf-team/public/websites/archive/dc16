import codecs
import sys

from django.core.management.base import BaseCommand

if sys.version_info >= (3,):
    import csv
else:
    from backports import csv

from dc16.models.front_desk import FrontDeskAttendee
from dc16.models import bursaries_group


class Command(BaseCommand):
    help = 'Dump bursary status'

    def handle(self, *args, **options):
        stream_writer = codecs.getwriter('utf-8')
        bytestream = getattr(sys.stdout, 'buffer', sys.stdout)
        csv_file = csv.writer(stream_writer(bytestream))
        csv_file.writerow((
            'username', 'name', 'email', 'bursary_source',
            'approve_debconf_bursary', 'approve_debcamp_bursary',
            'approve_travel_bursary', 'registered', 'arrived'))

        for user in FrontDeskAttendee.objects.all():
            approve_debconf_bursary = user.kv.filter(
                group=bursaries_group, key='approve_debconf_bursary').first()
            if approve_debconf_bursary:
                approve_debconf_bursary = approve_debconf_bursary.value
            approve_debcamp_bursary = user.kv.filter(
                group=bursaries_group, key='approve_debcamp_bursary').first()
            if approve_debcamp_bursary:
                approve_debcamp_bursary = approve_debcamp_bursary.value
            approve_travel_bursary = user.kv.filter(
                group=bursaries_group, key='approve_travel_bursary').first()
            if approve_travel_bursary:
                approve_travel_bursary = approve_travel_bursary.value

            if user.bursary_source():
                csv_file.writerow((
                    user.user.username, user.display_name(), user.user.email,
                    user.bursary_source(), approve_debconf_bursary,
                    approve_debcamp_bursary, approve_travel_bursary,
                    user.is_registered(), user.arrived()))
