from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group

from wafer.users.models import UserProfile


class Command(BaseCommand):
    help = 'Enable all the bursary flags for a user'

    def handle(self, *args, **options):
        username, = args

        reg_group = Group.objects.get_by_natural_key('Registration')

        user = UserProfile.objects.get(user__username=username)
        for event in ('debcamp', 'debconf'):
            for service in ('accommodation', 'food'):
                using = user.kv.get(group=reg_group,
                                    key='%s_%s' % (service, event))
                if not using.value:
                    continue
                bursary = user.kv.get(
                    group=reg_group, key='%s_bursary_%s' % (service, event))
                if bursary.value:
                    continue
                bursary.value = True
                bursary.save()
                print "Granted bursary for %s %s to %s" % (
                    event, service, username)
