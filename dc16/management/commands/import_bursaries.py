import codecs
import sys

from django.core.management.base import BaseCommand

from wafer.users.models import UserProfile

from dc16.models import KVAttendee, reg_group, bursaries_group

if sys.version_info >= (3,):
    import csv
else:
    from backports import csv


class Command(BaseCommand):
    help = "Import bursary status"

    def handle(self, *args, **options):
        stream_reader = codecs.getreader('utf-8')
        bytestream = getattr(sys.stdin, 'buffer', sys.stdin)
        csv_file = csv.reader(stream_reader(bytestream))

        header = next(csv_file)
        for row in csv_file:
            row = dict(zip(header, row))
            for key in ('approve_debconf_bursary',
                        'approve_debcamp_bursary'):
                row[key] = {
                    u'true': True,
                    u'false': False,
                    u'': None,
                }[row[key].lower()]

            if row['approve_travel_bursary']:
                row['approve_travel_bursary'] = int(
                    row['approve_travel_bursary'])

            user = UserProfile.objects.get(user__username=row['username'])

            for key in ('approve_debconf_bursary', 'approve_debcamp_bursary',
                        'approve_travel_bursary', 'round'):
                user.kv.update_or_create(group=bursaries_group(),
                                         key=key,
                                         defaults={'value': row[key]})
            kv, created = user.kv.get_or_create(
                group=reg_group(),
                key='bursary_source',
                defaults={'value': row['bursary_source']})
            if not created and kv.value != row['bursary_source']:
                print "Updating %s bursary_source from %s to %s" % (
                    row['username'], kv.value, row['bursary_source'])
                kv.value = row['bursary_source']
                kv.save()

            def reg(key):
                return user.kv.get(group=reg_group(), key=key).value

            if row['approve_debconf_bursary'] and not (
                    reg('accommodation_bursary_debconf') or
                    reg('food_bursary_debconf')):
                print("DebConf bursary granted but not requested for %s"
                      % row['username'])

            if row['approve_debcamp_bursary'] and not (
                    reg('accommodation_bursary_debcamp') or
                    reg('food_bursary_debcamp')):
                print("DebCamp bursary granted but not requested for %s"
                      % row['username'])
