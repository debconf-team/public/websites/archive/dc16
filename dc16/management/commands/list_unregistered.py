from django.core.management.base import BaseCommand

from wafer.users.models import UserProfile
from dc16.models import reg_group


class Command(BaseCommand):
    help = ("List users who have filled in the registration form, but "
            "aren't registered")

    def handle(self, *args, **options):
        self.reg_group = reg_group()
        for user_profile in UserProfile.objects.all():
            if self.check_user(user_profile):
                print user_profile.user.username

    def check_user(self, user_profile):
        if user_profile.is_registered():
            return False
        if user_profile.kv.filter(
                group=self.reg_group,
                key='travel_bursary').exists():
            return True
        return False
