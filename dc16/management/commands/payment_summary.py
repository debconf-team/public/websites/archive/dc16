from django.core.management.base import BaseCommand

from dc16.models.accounts import PayingAttendee


class Command(BaseCommand):
    help = 'Summarise payment status'

    def handle(self, *args, **options):
        invoiced = 0
        collected = 0
        expected = 0
        expected_invoiced = 0

        for user in PayingAttendee.objects.all():
            bill, description = user._bill()
            paid, description = user._payments()
            status = user.latest_payment_status()
            expected += bill
            if status is None:
                continue
            elif status in ('paid', 'refunded'):
                collected += paid
            elif status == 'invoiced':
                invoiced += paid
                expected_invoiced += bill
            else:
                raise ValueError("Unknown status: %s" % status)

        print 'Expected total:      R %10.2f' % expected
        print 'Expected receivable: R %10.2f' % expected_invoiced
        print 'Receivable:          R %10.2f' % invoiced
        print 'Received:            R %10.2f' % collected
