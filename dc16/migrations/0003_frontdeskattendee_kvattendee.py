# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20160329_2003'),
        ('dc16', '0002_payingattendee'),
    ]

    operations = [
        migrations.CreateModel(
            name='KVAttendee',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('users.userprofile',),
        ),
        migrations.CreateModel(
            name='FrontDeskAttendee',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('dc16.kvattendee',),
        ),
    ]
