from django import forms
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse

from wafer.kv.utils import deserialize_by_field
from wafer.users.models import UserProfile

from dc16.registration import RegistrationForm


def reg_group():
    return Group.objects.get_by_natural_key('Registration')


def accounts_group():
    return Group.objects.get_by_natural_key('Accounts')


def bursaries_group():
    return Group.objects.get_by_natural_key('Bursaries')


def reg_field(key, field):
    def wrapped(self):
        value = self._kv.get(key)
        return deserialize_by_field(value, field)
    if isinstance(field, forms.BooleanField):
        wrapped.boolean = True
    wrapped.short_description = key.replace('_', ' ')
    return wrapped


class KVAttendee(UserProfile):
    class Meta:
        proxy = True

    def __init__(self, *args, **kwargs):
        super(KVAttendee, self).__init__(*args, **kwargs)
        self._kv = {}
        group_id = reg_group().id
        for kv in self.kv.all():
            if kv.group_id != group_id:
                continue
            self._kv[kv.key] = kv.value

    # User proxy fields
    def email(self):
        return self.user.email

    def username(self):
        return self.user.username

    # This isn't crazy. At all...
    for name, field in RegistrationForm().fields.items():
        locals()[name] = reg_field(name, field)

    def get_absolute_url(self):
        return reverse('wafer_user_profile', args=(self.user.username,))


from dc16.models.accounts import PayingAttendee
from dc16.models.front_desk import FrontDeskAttendee
