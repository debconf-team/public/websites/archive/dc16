from datetime import timedelta

from django.db import models
from django.utils.dateparse import parse_datetime
from django.utils.timezone import localtime, now

from dc16.models import KVAttendee, accounts_group, bursaries_group, reg_group


USD_ZAR = 15  # Approx exchange rate in March/April 2016
DEBCAMP_START = parse_datetime('2016-06-23 10:00:00+02:00')
DEBCONF_START = parse_datetime('2016-07-02 10:00:00+02:00')
DEBCONF_END = parse_datetime('2016-07-10 10:00:00+02:00')


class PayingAttendeeManager(models.Manager):
    def get_queryset(self):
        # I'll be the first to admit that this is a downright horrible hack
        # it's about as inefficient as one can be.
        # Just as well we're small :)
        qs = super(PayingAttendeeManager, self).get_queryset()
        return qs.filter(id__in=[
            attendee.id for attendee in qs.all()
            if attendee.is_payable()]).prefetch_related('kv')


def count_nights(arrival, departure):
    arrival_day = arrival.date()
    departure_day = departure.date()
    qualifiers = []
    if arrival.hour < 4:
        arrival_day -= timedelta(days=1)
        qualifiers.append('Early arrival, including night of %i'
                          % arrival.day)
    if departure.hour < 4:
        departure_day -= timedelta(days=1)
        qualifiers.append('Early departure, excluding night of %i'
                          % departure.day)
    nights = (departure_day - arrival_day).days
    return nights, qualifiers


class PayingAttendee(KVAttendee):
    class Meta:
        proxy = True

    objects = PayingAttendeeManager()

    def has_bursary(self, event):
        kv = self.kv.filter(
            group=bursaries_group,
            key='approve_%s_bursary' % event,
        ).first()
        if kv:
            return kv.value
        return False

    def is_payable(self):
        if self.payments():
            return True

        if not self.is_registered():
            return False

        paying_debcamp = ((
            self.accommodation_debcamp() or
            self.food_debcamp()) and not
            self.has_bursary('debcamp'))
        paying_debconf = ((
            self.accommodation_debconf() or
            self.food_debconf()) and not
            self.has_bursary('debconf'))

        paying = self.fee() or paying_debcamp or paying_debconf

        if paying:
            return True

        return False

    is_payable.boolean = True

    def paid(self):
        return self._payments()[0] >= self._bill()[0]

    paid.boolean = True

    def payments(self):
        return self._payments()[1]

    def nights(self):
        nights, qualifiers = count_nights(self.arrival(), self.departure())
        if qualifiers:
            return '%i (%s)' % (nights, ', '.join(qualifiers))
        return nights

    def total_fee(self):
        return self._bill()[1]

    def _bill(self):
        bill = Bill()

        fee, fee_description = {
            '': (0, u'No conference fee'),
            'pro': (200 * USD_ZAR, u'Professional conference fee - $200'),
            'corp': (500 * USD_ZAR, u'Corporate conference fee - $500'),
        }[self.fee()]
        if fee:
            fee_description += u' at %s ZAR/USD' % USD_ZAR

        if self.kv.filter(
                group=reg_group(), key='complimentary_ticket').exists():
            fee = 0
            fee_description = ('Complimentary %s registration - R 0'
                               % fee_description.split()[0])

        bill.add_item(fee, fee_description)

        debcamp_bursary = self.has_bursary('debcamp')
        debconf_bursary = self.has_bursary('debconf')

        debcamp_nights = count_nights(
            max(self.arrival(), DEBCAMP_START),
            min(self.departure(), DEBCONF_START))
        debcamp_early_nights = count_nights(self.arrival(), DEBCAMP_START)
        debcamp_late_nights = count_nights(DEBCONF_START, self.departure())
        if self.accommodation_debcamp():
            if not debcamp_bursary:
                bill.add_nights(debcamp_nights, 'nights of DebCamp')
                bill.add_nights(debcamp_early_nights, 'nights before DebCamp')
            if not self.debconf():
                bill.add_nights(debcamp_late_nights, 'nights after DebCamp')
        if self.food_debcamp():
            if not debcamp_bursary:
                bill.add_nights(debcamp_nights, 'days of DebCamp food')
            bill.add_warning(debcamp_early_nights, 'no food before DebCamp')
            if not self.debconf():
                bill.add_nights(debcamp_late_nights,
                                'days of food after DebCamp')

        debconf_nights = count_nights(
            max(self.arrival(), DEBCONF_START),
            min(self.departure(), DEBCONF_END))
        debconf_early_nights = count_nights(self.arrival(), DEBCONF_START)
        debconf_late_nights = count_nights(DEBCONF_END, self.departure())
        if self.accommodation_debconf():
            if not debconf_bursary:
                bill.add_nights(debconf_nights, 'nights of DebConf')
            if not self.debcamp():
                bill.add_nights(debconf_early_nights, 'nights before DebConf')
            bill.add_warning(debconf_late_nights,
                             'no accommodation after DebConf')
        if self.food_debconf():
            if not debconf_bursary:
                bill.add_nights(debconf_nights, 'days of DebConf food')
            if not self.debcamp():
                bill.add_nights(debconf_early_nights,
                                'days of food before DebConf')
            bill.add_warning(debconf_late_nights,
                             'no food after DebConf')

        return bill.total(), unicode(bill)

    def process_payment(self, status, amount, method, reference, notes):
        payments, created = self.kv.get_or_create(
            group=accounts_group(), key='payments', defaults={'value': []})
        payment = {
            'status': status,
            'date': now(),
            'notes': notes,
        }

        if status in ('paid', 'refunded'):
            payment.update({
                'amount': amount,
                'method': method,
                'ref': reference,
            })

        payments.value.append(payment)
        payments.save()

    def _payments(self):
        payments = self.kv.filter(
            group=accounts_group(), key='payments').first()
        if not payments:
            return (0, None)

        lines = []
        total = 0
        for payment in payments.value:
            date = localtime(parse_datetime(payment['date']))
            line = date.strftime(u'%Y-%m-%d %H:%M:%S')
            line += u' %(status)s' % payment
            if payment.get('amount'):
                line += u' R %(amount)0.2f' % payment
            if payment.get('method'):
                line += u' (%(method)s)' % payment
            if payment.get('ref'):
                line += u' Ref: %(ref)s' % payment
            if payment.get('notes'):
                line += u' - Notes: %(notes)s' % payment
            lines.append(line)
            if payment['status'] in ('paid', 'refunded'):
                total += payment['amount']

        lines.append(u'Total: R %0.2f' % total)
        return total, u'\n'.join(lines)

    def latest_payment_status(self):
        payments = self.kv.filter(
            group=accounts_group(), key='payments').first()
        if not payments:
            return None
        return payments.value[-1]['status']


class Bill(object):
    def __init__(self):
        self.lineitems = []

    def add_item(self, price, description):
        self.lineitems.append((price, description))

    def add_nights(self, count_result, description):
        qty, qualifiers = count_result
        if qty < 1:
            return
        fee = qty * 300  # Both food & accom are R300/day
        self.add_item(fee, u'%i %s @ R300' % (qty, description))
        for line in qualifiers:
            self.add_item(0, u' - %s' % line)

    def add_warning(self, count_result, description):
        qty, qualifiers = count_result
        if qty < 1:
            return
        self.add_item(0, u'WARNING: %s' % description)
        for line in qualifiers:
            self.add_item(0, u' - %s' % line)

    def total(self):
        return sum(line[0] for line in self.lineitems)

    def __unicode__(self):
        lines = [line[1] for line in self.lineitems]
        lines.append(u'Total: R %i' % self.total())
        return u'\n'.join(lines)
