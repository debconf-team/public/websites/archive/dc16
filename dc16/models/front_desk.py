from django import forms
from django.contrib.auth.models import Group

from wafer.kv.utils import deserialize_by_field

from dc16.forms import FrontDeskForm, RoomForm
from dc16.models import KVAttendee
from dc16.models import PayingAttendee


def fd_group():
    return Group.objects.get_by_natural_key('Front Desk')


def fd_field(key, field):
    def wrapped(self):
        value = self._fd.get(key)
        return deserialize_by_field(value, field)
    if isinstance(field, forms.BooleanField):
        wrapped.boolean = True
    wrapped.short_description = key.replace('_', ' ')
    return wrapped


class FrontDeskAttendee(KVAttendee):
    class Meta:
        proxy = True

    def __init__(self, *args, **kwargs):
        super(FrontDeskAttendee, self).__init__(*args, **kwargs)
        self._fd = {}
        for kv in self.kv.filter(group=fd_group()):
            self._fd[kv.key] = kv.value

    # This isn't crazy. At all...
    for name, field in FrontDeskForm().fields.items():
        locals()[name] = fd_field(name, field)

    for name, field in RoomForm().fields.items():
        locals()[name] = fd_field(name, field)

    def process_fd_form(self, form_data):
        group = fd_group()
        for key, value in form_data.items():
            kv, created = self.kv.get_or_create(
                group=group, key=key, defaults={'value': {}})

            kv.value = value
            kv.save()

    def paid(self):
        try:
            return PayingAttendee.objects.get(id=self.id).paid()
        except PayingAttendee.DoesNotExist:
            return None
    paid.boolean = True
