from django import forms
from django.conf import settings

from crispy_forms.bootstrap import PrependedAppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Fieldset, Layout, Submit

FOOD_LINK = ('<a href="https://wiki.debconf.org/wiki/DebConf16/Food" '
             'target="blank">More information</a>')
ACCOM_LINK = ('<a href="https://wiki.debconf.org/wiki/DebConf16/Accomodation" '
              'target="blank">More information</a>')
BURSARIES_LINK = ('<a href="/about/bursaries" target="blank">'
                  'DebConf bursary instructions</a>')
BURSARIES_CLOSED_MSG = (
    'Bursary applications have closed. '
    'It is too late to apply for a bursary.'
)
BURSARIES_OPEN = frozenset(('local',))


def tshirt_sizes():
    yield None, 'No t-shirt, please'
    for size in ('xs', 's', 'm', 'l', 'xl', '2xl', '3xl', '4xl', '5xl'):
        for cut in ('straight', "women's fitted"):
            short = '%s:%s' % (cut[0], size)
            label = '%s (%s cut)' % (size.upper(), cut)
            yield short, label


class RegistrationForm(forms.Form):
    send_email_confirmation = True

    debcamp = forms.BooleanField(
        label='I plan to attend DebCamp (23 June to 1 July)',
        required=False)
    debconf = forms.BooleanField(
        label='I plan to attend DebConf (2 July to 9 July)',
        required=False)
    fee = forms.ChoiceField(
        label='My registration fee',
        choices=(
            ('', 'Basic attendee - Free'),
            ('pro', 'Professional level - $200'),
            ('corp', 'Corporate level - $500'),
        ),
        help_text='Prices in USD. We encourage attendees to pay for their '
                  'attendance if they can afford to do so.',
        widget=forms.RadioSelect,
        required=False)
    arrival = forms.DateTimeField(
        label='I arrive at the venue on',
        help_text="Please estimate, if you haven't booked tickets, yet, "
                  'and update it when you have final dates.',
    )
    departure = forms.DateTimeField(
        label='I depart from the venue on',
    )
    final_dates = forms.BooleanField(
        label='My travel dates are final, please book my accommodation and '
              'bill me',
        help_text="If you've applied for a bursary, we won't bill you, "
                  "until we are sure you're going to have to pay. "
                  "But it's still useful to know that your dates are final.",
        required=False,
    )
    reconfirm = forms.BooleanField(
        label='I reconfirm my attendance',
        help_text="If you do not select this by May 20th, we'll assume you "
                  "aren't coming",
        required=False,
    )

    nickname = forms.CharField(
        label='Nickname on my badge',
        help_text='Debian username / IRC nick / social media handle.',
        required=False)
    organization = forms.CharField(
        label='My Group, team, or organization',
        help_text='Or anything else you want on your badge. '
                  'This is the third line of your badge.',
        required=False)
    t_shirt = forms.ChoiceField(
        label='My T-shirt size',
        choices=tshirt_sizes(),
        required=False)
    gender = forms.ChoiceField(
        label='My gender',
        choices=(
            ('', 'Decline to state'),
            ('m', 'Male'),
            ('f', 'Female'),
            ('o', 'Other'),
        ),
        help_text='For diversity statistics',
        required=False)
    diet = forms.ChoiceField(
        label='My diet',
        choices=(
            ('', 'I will be happy to eat whatever is provided'),
            ('pescatarian', "I am pescatarian, don't provide meat, "
                            "but I'll eat fish"),
            ('vegetarian', "I am lacto-ovo vegetarian, don't provide "
                           "meat/fish for me"),
            ('vegan', "I am strict vegatarian (vegan), don't provide any "
                      "animal products for me"),
            ('other', 'Other, described below'),
        ),
        required=False)
    special_needs = forms.CharField(
        label='My special needs',
        help_text='Wheelchair access, food allergies, other diets, etc.',
        required=False)
    emergency_contact = forms.CharField(
        label='My emergency contact',
        help_text='Please include the name, phone number, and language spoken '
                  '(if not English)',
        widget=forms.Textarea(attrs={'rows': 3}),
        required=False)

    accommodation_debcamp = forms.BooleanField(
        label='I need accommodation at the venue',
        help_text='self-paid or sponsored. ' + ACCOM_LINK,
        required=False)
    food_debcamp = forms.BooleanField(
        label='I need conference-provided food',
        help_text='self-paid or sponsored. ' + FOOD_LINK,
        required=False)
    accommodation_bursary_debcamp = forms.BooleanField(
        label='I request a bursary (sponsorship) for accommodation',
        required=False)
    food_bursary_debcamp = forms.BooleanField(
        label='I request a bursary (sponsorship) for food',
        required=False)

    accommodation_debconf = forms.BooleanField(
        label='I need accommodation at the venue',
        help_text='self-paid or sponsored. ' + ACCOM_LINK,
        required=False)
    food_debconf = forms.BooleanField(
        label='I need conference-provided food',
        help_text='self-paid or sponsored. ' + FOOD_LINK,
        required=False)
    accommodation_bursary_debconf = forms.BooleanField(
        label='I request a bursary (sponsorship) for accommodation',
        help_text=BURSARIES_LINK,
        required=False)
    food_bursary_debconf = forms.BooleanField(
        label='I request a bursary (sponsorship) for food',
        help_text=BURSARIES_LINK,
        required=False)

    bursary_source = forms.ChoiceField(
        label='I am applying for a bursary from',
        choices=(
            ('', 'N/A'),
            ('debian', 'Debian (Active Debian contributor)'),
            ('gsoc', 'GSoC/Outreachy (2016 GSoC / Outreachy intern)'),
            ('diversity', 'Debian Diversity Outreach'),
            ('local', 'Local Team Volunteer'),
        ),
        help_text='The budget that my bursary will come from',
        required=False)
    travel_bursary = forms.IntegerField(
        label='I request a bursary for travel',
        help_text='Estimated amount required. ' + BURSARIES_LINK,
        min_value=0,
        max_value=10000,
        required=False)
    bursary_reason = forms.CharField(
        label='Details of my bursary request',
        help_text='This is where you explain your needs, and involvement in '
                  'Debian, that justify a bursary. See the ' + BURSARIES_LINK,
        widget=forms.Textarea(attrs={'rows': 5}),
        required=False)
    bursary_need = forms.ChoiceField(
        label='My level of need',
        choices=(
            ('', 'N/A (not requesting a travel bursary)'),
            ('unable', 'Without this funding I will be absolutely '
                       'unable to attend'),
            ('sacrifice', 'Without the requested funding I will have to '
                          'make financial sacrifices, to attend'),
            ('inconvenient', 'Without the requested funding attending will '
                             'be inconvenient for me'),
            ('non-financial', 'I am not applying based on financial need'),
        ),
        required=False)

    family_size = forms.IntegerField(
        label='My accompanying family size',
        help_text='We will try to find suitable accommodation. '
                  'It may be off-campus. '
                  'Rooms in the university accommodation are mostly single, '
                  'but there are some with two beds.',
        min_value=1,
        max_value=10,
        required=False)
    family_usernames = forms.CharField(
        label='Usernames of my family members, '
              'who have registered separately',
        help_text="One per line. This isn't validated",
        widget=forms.Textarea(attrs={'rows': 3}),
        required=False)
    children_details = forms.CharField(
        label='More details about my children',
        help_text='Please include ages and any other pertinent information. '
                  'Be aware that South Africa has strict requirements for '
                  'parents travelling with children. Contact the '
                  '<a href="/about/visiting-south-africa" target="blank">'
                  'visa team</a> for details.',
        widget=forms.Textarea(attrs={'rows': 3}),
        required=False)

    session_planned = forms.BooleanField(
        label='I plan to submit a session for DebConf',
        help_text=(
            'Talk submissions are open. Submit now, and beat the rush'
            if settings.WAFER_TALKS_OPEN else
            "Don't forget to do this!"
        ),
        required=False)
    role = forms.MultipleChoiceField(
        label='My role at DebConf',
        choices=(
            ('volunteer', 'DebConf volunteer'),
            ('orga', 'DebConf organisational team member '
                     '(currently involved in DC16 orga)'),
            ('video', 'DebConf video team volunteer'),
            ('press', 'Member of the press (Please contact us)'),
        ),
        widget=forms.CheckboxSelectMultiple,
        required=False)

    billing_address = forms.CharField(
        label='My billing address',
        widget=forms.Textarea(attrs={'rows': 3}),
        required=False)

    notes = forms.CharField(
        label='Notes for the registration team',
        help_text='Anything else you need to describe',
        widget=forms.Textarea(attrs={'rows': 3}),
        required=False)

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'registration'
        self.helper.include_media = False
        self.helper.layout = Layout(
            Fieldset(
                'Registration',
                'debcamp',
                'debconf',
                'fee',
                'arrival',
                'departure',
                'final_dates',
                'reconfirm',
            ),
            Fieldset(
                'About me',
                'nickname',
                'organization',
                't_shirt',
                'gender',
                'diet',
                'special_needs',
                'emergency_contact',
            ),
            Fieldset(
                'DebCamp (23 June to 1 July)',
                'accommodation_debcamp',
                'food_debcamp',
                'accommodation_bursary_debcamp',
                'food_bursary_debcamp',
            ),
            Fieldset(
                'DebConf (2 July to 9 July)',
                'accommodation_debconf',
                'food_debconf',
                'accommodation_bursary_debconf',
                'food_bursary_debconf',
            ),
            Fieldset(
                'Bursaries',
                'bursary_source',
                PrependedAppendedText('travel_bursary', 'US $', '.00',
                                      style='text-align: right'),
                'bursary_reason',
                'bursary_need',
            ),
            Fieldset(
                'Family',
                HTML(
                    '<p>'
                    'Members of your family should register separately if '
                    'they are:'
                    '</p>'
                    '<ul>'
                    '<li>a conference volunteer</li>'
                    '<li>a speaker</li>'
                    '<li>requesting bursaries</li>'
                    '</ul>'
                    '<p>'
                    'If not, you can simply describe your family here.'
                    '</p>'
                ),
                'family_size',
                'family_usernames',
                'children_details',
            ),
            Fieldset(
                'Involvement at DebConf',
                'session_planned',
                'role',
            ),
            Fieldset(
                'Paid attendees',
                'billing_address',
            ),
            Fieldset(
                'And, finally',
                'notes',
            ),
        )

        self.helper.add_input(Submit('submit', 'Save'))

    def initial_values(self, user):
        # A hook, hopefully available in wafer > 0.3.2
        # https://github.com/CTPUG/wafer/pull/212
        return {
            'nickname': user.user.username,
        }

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        paid = bool(cleaned_data.get('fee'))
        bursary_source = cleaned_data.get('bursary_source')
        any_bursary = False

        if paid and not (cleaned_data.get('debcamp') or
                         cleaned_data.get('debconf')):
            self.add_error(
                'fee',
                "We can't collect a conference fee, if you aren't registering "
                "attendance at the conference")

        for event in ('debcamp', 'debconf'):
            attending = cleaned_data.get(event)
            for service in ('accommodation', 'food'):
                using_key = '%s_%s' % (service, event)
                bursary_key = '%s_bursary_%s' % (service, event)
                using = cleaned_data.get(using_key)
                bursary = cleaned_data.get(bursary_key)
                if using and not attending:
                    self.add_error(
                        using_key,
                        '"%s" must also be selected'
                        % self.fields[event].label)
                if bursary and not using:
                    self.add_error(
                        bursary_key,
                        '"%s" must also be selected'
                        % self.fields[using_key].label)
                if using and not bursary:
                    paid = True
                if (bursary and not self.initial.get(bursary_key) and
                        bursary_source not in BURSARIES_OPEN):
                    self.add_error(bursary_key, BURSARIES_CLOSED_MSG)
                if bursary:
                    any_bursary = True
                if using and not cleaned_data.get('final_dates'):
                    self.add_error(
                        'final_dates',
                        'At this stage of registration, your dates must be '
                        'final.')

        if cleaned_data.get('travel_bursary') == 0:
            cleaned_data['travel_bursary'] = None

        if cleaned_data.get('travel_bursary'):
            any_bursary = True
            if not cleaned_data.get('bursary_reason'):
                self.add_error(
                    'bursary_reason',
                    'A travel bursary has been requested, '
                    'please explain why it is needed')
            if not cleaned_data.get('bursary_need'):
                self.add_error(
                    'bursary_need',
                    'A travel bursary has been requested, '
                    'please explain the level of need')
            if (cleaned_data.get('travel_bursary') >
                    self.initial.get('travel_bursary') and
                    bursary_source not in BURSARIES_OPEN):
                self.add_error('travel_bursary', BURSARIES_CLOSED_MSG)

        if any_bursary and not bursary_source:
            self.add_error(
                'bursary_source',
                'If you are applying for a bursary, you need to select the '
                'appropriate bursary source.'
            )

        if (bursary_source and
                bursary_source != self.initial.get('bursary_source') and
                bursary_source not in BURSARIES_OPEN):
            self.add_error('bursary_source', BURSARIES_CLOSED_MSG)

        if ((cleaned_data.get('accommodation_bursary_debcamp') or
                cleaned_data.get('food_bursary_debcamp')) and
                not cleaned_data.get('bursary_reason')):
            self.add_error(
                'bursary_reason',
                'A debcamp bursary has been requested, '
                'please explain why it is needed')

        if (cleaned_data.get('diet') == 'other' and
                not cleaned_data.get('special_needs')):
            self.add_error('special_needs', 'Required when diet is "other"')

        if paid and not cleaned_data.get('billing_address'):
            self.add_error('billing_address',
                           'Paid attendees need to provide a billing address')

        if cleaned_data.get('final_dates'):
            for field in ('arrival', 'departure'):
                if not cleaned_data.get(field):
                    self.add_error(
                        field, 'If your dates are final, pleas provide them')

        if cleaned_data.get('debconf') or cleaned_data.get('debcamp'):
            if not cleaned_data.get('reconfirm'):
                self.add_error(
                    'reconfirm',
                    'At this stage of registration, '
                    'you must reconfirm immediately.')

    @classmethod
    def is_registered(cls, kv_data):
        """
        Given a user's kv_data query, determine if they have registered to
        attend.
        """
        for item in kv_data.filter(key__in=('debcamp', 'debconf')):
            if item.value is True:
                return True
        return False
