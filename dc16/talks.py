from wafer.talks import forms


class TalkForm(forms.TalkForm):
    def __init__(self, *args, **kwargs):
        self.base_fields['authors'].label = 'Speakers'
        self.base_fields['abstract'].label = 'Description'
        super(TalkForm, self).__init__(*args, **kwargs)
