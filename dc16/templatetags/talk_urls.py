from django.template import Library

register = Library()


@register.filter
def has_video(talk_urls):
    """
    Return true if the talk has a video
    """
    found = False
    for talk_url in talk_urls:
        if talk_url.description == 'Video':
            found = True
    return found
