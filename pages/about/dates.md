---
name: Dates
---
<h1 class="fancy-heading"><span>Important Dates</span></h1>

<table class="table table-condensed" style="background-color: #fff;">
        <tbody>
        <tr>
            <td colspan="2" style="font-weight: bold; background-color: #bbbed7;">March 2016</td>
        </tr>
        <tr>
            <td rowspan="2">Friday, 18 March</td>
            <td><a href="/accounts/profile/">Registration opens</a></td>
        </tr>
        <tr>
            <td><a href="/talks/new/">Call for Papers opens</a></td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; background-color: #bbbed7;">April 2016</td>
        </tr>
        <tr>
            <td>Sunday, 10 April</td>
            <td>Sponsorship application deadline</td>
        </tr>
        <tr>
            <td></td>
            <td>Deadline to a guarantee accommodation at the venue</td>
        </tr>
        <tr>
            <td rowspan="2">mid April</td>
            <td><a href="/cfp/">Announces of first batch of accepted talks</a></td>
        </tr>
        <tr>
            <td>Accommodation application deadline</td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; background-color: #bbbed7;">May 2016</td>
        </tr>
        <tr>
            <td>Sunday, 1 May</td>
            <td><a href="/cfp/">Close of talk submission</a></td>
        <tr>
            <td colspan="2" style="font-weight: bold; background-color: #bbbed7;">June 2016</td>
        </tr>
        <tr>
            <td>Thursday, 23 June</td>
            <td>Start of DebCamp, University accommodation available from this day</td>
        </tr>
        <tr>
            <td>Friday, 24 June</td>
            <td>Second day of DebCamp: Set-up</td>
        </tr>
        <tr>
            <td>Saturday, 25 June</td>
            <td>Third day of DebCamp</td>
        </tr>
        <tr>
            <td>Sunday, 26 June</td>
            <td>Fourth day of DebCamp</td>
        </tr>
        <tr>
            <td>Monday, 27 June</td>
            <td>Fifth day of DebCamp</td>
        </tr>
        <tr>
            <td>Tuesday, 28 June</td>
            <td>Sixth day of DebCamp</td>
        </tr>
        <tr>
            <td>Wednesday, 29 June</td>
            <td>Seventh day of DebCamp</td>
        </tr>
        <tr>
            <td>Thursday, 30 June</td>
            <td>Eighth day of DebCamp, Cocktail Party</td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; background-color: #bbbed7;">July 2016</td>
        </tr>
        <tr>
            <td>Friday, 1 July</td>
            <td>Final day of DebCamp</td>
        </tr>
        <tr>
            <td>Saturday, 2 July</td>
            <td>Arrival day for DebConf, First day for sponsored people, Open Weekend</td>
        </tr>
        <tr>
            <td>Sunday, 3 July</td>
            <td>First day of DebConf: Open Weekend, Debian Day</td>
        </tr>
        <tr>
            <td>Monday, 4 July</td>
            <td>Second day of DebConf: <a href="https://wiki.debconf.org/wiki/DebConf16/CheeseWineBoF">Cheese and Wine Party</a></td>
        </tr>
        <tr>
            <td>Tuesday, 5 July</td>
            <td>Third day of DebConf</td>
        </tr>
        <tr>
            <td>Wednesday, 6 July</td>
            <td>Fourth day of DebConf: Day Trip</td>
        </tr>
        <tr>
            <td>Thursday, 7 July</td>
            <td>Fifth day of DebConf</td>
        </tr>
        <tr>
            <td>Friday, 8 July</td>
            <td>Sixth day of DebConf, Debconf Dinner</td>
        </tr>
        <tr>
            <td>Saturday, 9 July</td>
            <td>Last day of DebConf: Closing ceremony, Teardown</td>
        </tr>
        <tr>
            <td>Sunday, 10 July</td>
            <td>Departure day, all people should leave the venue by 10:00</td>
        </tr>
        </tbody>
</table>

