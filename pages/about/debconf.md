---
name: About DebConf
---
<h1 class="fancy-heading"><span>About DebConf</span></h1>

DebConf is the annual [Debian][] developers meeting, an event filled with discussions, workshops and coding parties — all of them highly technical in nature.
DebConf16, the 17th Debian Conference, will be held in Cape Town, South Africa, from **2 July to 9 July 2016** at the **University of Cape Town**.

This year's schedule of events will be exciting, productive and fun.
As in previous years, DebConf16 features speakers from around the world.
Past Debian Conferences have been extremely beneficial for developing key components of the Debian system, infrastructure and community.

<h2 class="fancy-heading"><span>Venue</span></h2>

DebConf will be held on UCT's upper campus.

<a class="btn btn-default" role="button" href="https://wiki.debconf.org/wiki/DebConf16/Directions"><i class="fa fa-fw fa-external-link"></i> Directions</a>

<img class="img-responsive" src="/static/img/uct-landscape.jpg" alt="">

<h3 class="fancy-heading"><span>Maps</span></h3>

* [OpenStreetMap](http://www.openstreetmap.org/#map=18/-33.95777/18.46052)
* [UCT Campus](https://www.uct.ac.za/images/uct.ac.za/contact/campusmaps/big/uctuppercampus.jpg)
* Our venues:

<img class="img-responsive" src="/static/img/snape-menzies.png" alt="">

<h2 class="fancy-heading"><span>Codes of Conduct and Anti-harassment</span></h2>

DebConf is committed to a safe environment for all participants. All attendees are expected to treat all people and facilities with respect and help create a welcoming environment.
If you notice behaviour that fails to meet this standard, please speak up and help to keep DebConf as respectful as we expect it to be.

If you are harassed and requests to stop are not successful, or notice a disrespectful environment, the organizers want to help.
Please contact us at <antiharassment@debian.org>.
We will treat your request with dignity and confidentiality, investigate, and take whatever actions appropriate.
We can provide information on security, emergency services, transportation, alternative accommodations, or whatever else may be necessary.
If mediation is not successful, DebConf reserves the right to to take action against those who do not cease unacceptable behaviour.

See the [DebConf Code of Conduct] and the [Debian Code of Conduct]

<h2 class="fancy-heading"><span>Contacts</span></h2>

- **Local and global team**: DebConf Team mailing list. For any non-specific-team questions: (open subscription and public archives): <debconf-team@lists.debconf.org></dd>
- **Accommodation**: All questions and inquiries regarding accommodation, food preferences and special needs should be directed to the Room Assignments Team: <registration@debconf.org>
- **Registration**: All questions and inquiries regarding registration should be directed to the Registration Team: <registration@debconf.org>
- **Talks**: Questions and inquiries for talks and paper submissions: <talks@debconf.org>
- **Sponsorship (fundraising)**: Questions an inquiries about sponsorship and becoming a sponsor: <sponsors@debconf.org>
- **Visa**: Legal responsibilities regarding visa, conference invitations fall on our Visa Team <visa@debconf.org>
- **Harassment**: Report harassment and anything contributing to a disrespectful environment <antiharassment@debian.org>

<h3 class="fancy-heading"><span>Public contacts</span></h3>

- This site
- The [wiki pages about DebConf16], with further information.
- Mailing lists:
    - Participant discussions: <debconf-discuss@lists.debconf.org>, [subscriptions](http://lists.debconf.org/mailman/listinfo/debconf-discuss), [archives](http://lists.debconf.org/lurker/list/debconf-discuss.en.html)
    - Organizer discussions: <debconf-team@lists.debconf.org>, [subscriptions](http://lists.debconf.org/mailman/listinfo/debconf-team), [archives](http://lists.debconf.org/lurker/list/debconf-team.en.html)
- IRC on irc://irc.debian.org
    - General discussion: [`#debconf`](https://webchat.oftc.net/?channels=debconf)
    - DebConf team: [`#debconf-team`](https://webchat.oftc.net/?channels=debconf-team)

[Debian]: https://www.debian.org/
[DebConf Code of Conduct]:  http://debconf.org/codeofconduct.shtml
[Debian Code of Conduct]:  https://www.debian.org/code_of_conduct
[wiki pages about DebConf16]: https://wiki.debconf.org/wiki/DebConf16
