---
name: About Debian
---
<h1 class="fancy-heading"><span>About Debian</span></h1>

<img class="img-responsive" style="width:33%; float: right; padding: 5%;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Debian-OpenLogo.svg/775px-Debian-OpenLogo.svg.png" alt="" />

The Debian Project is an association of individuals who have made common cause to create a free operating system. This operating system that we have created is called Debian.

An operating system is the set of basic programs and utilities that make your computer run. At the core of an operating system is the kernel. The kernel is the most fundamental program on the computer and does all the basic housekeeping and lets you start other programs.

Debian systems currently use the Linux kernel or the FreeBSD kernel. Linux is a piece of software started by Linus Torvalds and supported by thousands of programmers worldwide. FreeBSD is an operating system including a kernel and other software.

Debian comes with over 43000 packages (pre-compiled software that is bundled up in a nice format for easy installation on your machine), a package manager (APT), and other utilities that make it possible to manage thousands of packages on thousands of computers as easily as installing a single application. All of it free.

Read more [on the Debian website](https://www.debian.org/intro/about).
