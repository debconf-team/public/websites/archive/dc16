---
name: Registration Information
---
<h1 class="fancy-heading"><span>Registration Information</span></h1>

Registration is open for DebConf16. The conference is taking place in Cape Town, South Africa from **Saturday, 2 July to Saturday, 9 July 2016** at the University of Cape Town.
Attendees must register by **Sunday, 10 April 2016** to apply for sponsored food, accommodation, or travel.
After this date, registrations will still be accepted in any of the basic, professional, and corporate categories, but requests for sponsorship will no longer be accepted, and accommodation at campus will no longer be guaranteed.
Register as soon as possible, to be sure of being accommodated! 

The conference will be held at the upper campus of the University of Cape Town in Newlands/Rondebosch, Cape Town, South Africa.
On-campus accommodations will be available to both sponsored and non-sponsored attendees at the on-campus Fuller (and if we grow >200 people, Smuts) residencies.
Meals will be served in the cafeteria of Fuller Hall, at UCT Club and possibly at other locations within the upper campus of UCT.
Additional external (non-DebConf) nearby accommodation are [listed in the wiki][external-accom].

[external-accom]: https://wiki.debconf.org/wiki/DebConf16/Accomodation/External

<h2 class="fancy-heading"><span>Registration fees</span></h2>

As always, basic registration for DebConf is free of charge for attendees.
If you are attending the conference in a professional capacity, or as a representative of your company, we ask that you consider registering in one of our paid categories, to help cover the costs of putting on the conference:

**Professional registration:** with a rate of 200 USD for the week, this covers the costs associated with your individual attendance at the conference.

**Corporate registration:** with a registration fee of 500 USD, this category is intended for those participating in DebConf as representatives of companies and helps to subsidise the cost of the conference for others.

As in previous three DebConfs, these registration fees **do not** include food or accommodation.

We encourage all attendees to help make DebConf a success by selecting the registration category appropriate for their situation.

<h3 class="fancy-heading"><span>Personal information</span></h3>

Due to the vagaries of the conference management system, your details have to be edited in a few different places. These are linked to from the right hand side of your profile page, and your registration form.

To edit your name and e-mail address, follow the "Edit User" link.
Your name (combining **First Name** (if set) and **Last Name**) is used as first line in the badge, as the speaker/moderator name on submitted events.
Additionally we will use such name to contact you, and for our lists (e.g. day trip options).

Your email address is the preferred address that DebConf registration team will use to contact you.

The **Nickname on my badge**  and **My group, team or organisation** registration fields are printed as second and third lines on your badge. 
To simplify identification, we recommend, to the Debian members and contributors, to include also the Debian (or Alioth) login name and IRC (if different).
But you are free to choose what you like (assuming it is not offensive, see the Codes of Conduct).

We recommend that you provide a contact phone number, this time behind the "Edit Profile" link.
This should be the number for your mobile phone that you carry at DebConf (if you do), and it will used only in emergency or if we can't find you and are worried about leaving you behind (e.g. during the day trip).

The **Emergency contact** should be the phone number and/or e-mail address of a friend or family member that we should contact, if something were to happen to you. We hope to never need this.

<h2 class="fancy-heading"><span>Payments and invoicing</span></h2>

Conference registration fees and accommodation need to be paid through UCT. You will be sent an invoice via email with instructions on how to settle by credit card or bank transfer to South Africa.

There is no option to pay the accommodation or food after Saturday, 30 April.
So please pay as soon as you received the invoice.

<h2 class="fancy-heading"><span>On-campus accommodation</span></h2>

Information about the university accommodation can be found on the [UCT website][UCT-residence]. The residence halls also have websites: [Fuller][], [Smuts][]
(these are aimed at, and run by, students, but contain useful information).

[UCT-residence]: http://www.uct.ac.za/apply/residence/uctresidence/first/residences/
[Fuller]: http://www.fullerhall.co.za/
[Smuts]: http://www.smutshall.com/

If you are not requesting sponsored accommodation, you still have the option of staying on campus in the dorms, in single room (very few double available, ask registration@debconf.org) with shared baths and showers.

The cost of accommodation at the venue is: R300 ≈ €18 ≈ $20 (US) per person per night.

Additional external (non-DebConf) nearby accommodation are [listed in the wiki][external-accom].

<h2 class="fancy-heading"><span>Food</span></h2>

Sponsored food will consist of three meals a day except on the field trip day when lunch will be replaced by a lunch box (goodie bag).

At the nearby UCT Club, the cost of meals is (per person):

-    Breakfast: R46 ≈ 2.60€ ≈ 2.84$ (US)
-    Lunch: from R51 ≈ 2.88€ ≈ 3.15$ (venue food) to R285 ≈ 16.10€ ≈ 17.60$ (the seafood platter, most expensive dish available)
-    Dinner: from R56 ≈ 3.33€ ≈ 3.64$ (venue food) to R285 ≈ 16.10€ ≈ 17.60$ (the same seafood platter that you did not eat during lunch :))
    
-    Kids 0-3 are free within the parents bed room (bring a crib).
-    Kids 4+ cost the rates given above regarding accommodation and food.
    
-    We are in the process of negotiating a contract with a caterer for food during DebConf. When final, lunch and dinner will be served for R300 per day with a choice of vegetarian or meat dishes each day.

We will try to cater for dairy and wheat free food (allergies), please state special requirements in the registration form.

<h2 class="fancy-heading"><span>Volunteering</span></h2>

Volunteers are always welcome! DebConf needs volunteers to help run the event. We need people to man the front desk, introduce speakers, operate video equipment, and much more.

If you are willing to volunteer, contact us by email at <debconf-team@lists.debconf.org>, or on the [`#debconf-team`][debconf-team-irc] IRC channel on `irc.debian.org`.

<h2 class="fancy-heading"><span>Sponsored attendance</span></h2>

Any member of the Debian community is welcome to request sponsorship to attend DebConf.
As in previous years, we will do our best to offer sponsored food and accommodation to Debian contributors, but sponsorship is subject to demand and available funding. Decisions regarding sponsorship requests are expected by the end of April, 2016.

See [bursaries] page for detailed information about sponsorship.
Please read it carefully.

[bursaries]: /about/bursaries/

<h2 class="fancy-heading"><span>DebCamp</span></h2>

DebCamp, in the days before DebConf,  provides an opportunity to contribute to Debian in a quiet environment, to collaborate with other developers, and to join Debian sprints and team meetings.

This year we have facilities for a reasonably large DebCamp, so we are not restricting DebCamp attendance based on work plans.
A solid working plan is needed only for [food and accommodation bursaries][bursaries].
For non-sponsored attendees, food and/or accommodation will be available at the venue for same cost as during DebConf.

Note that especially during the early parts of DebCamp, the DebConf team is focused on getting ready for DebConf, and facilities and organized services may be minimal.

<h2 class="fancy-heading"><span>Invitation letters</span></h2>

If you need a invitation letter, please send your request to <visa@debconf.org> and make sure to include your full name, passport number and expiration date as well as your physical address.

<h2 class="fancy-heading"><span>Disabilities and special needs</span></h2>

If you require special accommodation due to a disability, or have any other needs we can help with, please mail us at <registration@debconf.org>.

<h2 class="fancy-heading"><span>Anti-harassment and Codes of Conduct</span></h2>

DebConf is committed to a safe environment for all participants. All attendees are expected to treat all people and facilities with respect and help create a welcoming environment. If you notice behaviours that fails to meet this standard, please speak up and help to keep DebConf as respectful as we expect it to be.

If you are harassed and requests to stop are not successful, or notice a disrespectful environment, the organisers want to help. Please contact us at <antiharassment@debian.org>.
We will treat your request with dignity and confidentiality, investigate, and take whatever actions appropriate. We can provide information on security, emergency services, transportation, alternative accommodations, or whatever else may be necessary. If mediation is not successful, DebConf reserves the right to to take action against those who do not cease unacceptable behaviours.

See the [DebConf Code of Conduct] and the [Debian Code of Conduct]

[DebConf Code of Conduct]:  http://debconf.org/codeofconduct.shtml
[Debian Code of Conduct]:  https://www.debian.org/code_of_conduct


<h1 class="fancy-heading"><span>How to register</span></h1>

If you want to attend DebConf16, please fill out the **registration form**, linked from your [profile][].
[profile]: /accounts/profile/

As in recent years, the conference website is integrated with the Debian SSO system.
However, Debian SSO has changed, the new system is based on SSL Client certificates.
If you are a Debian developer and have not previously used the Debian SSO system, you will need to configure an SSO password on [db.debian.org]
If you are not a Debian developer, you will have an opportunity to [create an alioth account] as part of the registration process. For more information see [Debian single sign-on] page.
There is also the possibility to sign up without the SSO, but we highly recommend the the Debian community use SSO to sign up.

For any further questions related to registration please join [`#debconf-team`][debconf-team-irc] on OFTC or mail <registration@debconf.org>.

[db.debian.org]: https://db.debian.org
[create an alioth account]: https://alioth.debian.org/account/register.php
[Debian single sign-on]: https://wiki.debian.org/DebianSingleSignOn

<div class="text-center"><a href="/accounts/profile/" class="btn btn-primary btn-lg">Register Now</a></div>

<h2 class="fancy-heading"><span>Further steps</span></h2>

Consider [submitting a talk].

Consider [volunteering].

We suggest that attendees begin making travel arrangements as soon as possible.

[submitting a talk]: /cfp/
[volunteering]: /get-involved/

<h2 class="fancy-heading"><span>See also</span></h2>

- The [wiki pages about DebConf16], with further information:
- [Donations]
- [Important dates]

[wiki pages about DebConf16]: https://wiki.debconf.org/wiki/DebConf16
[Donations]: /get-involved/
[important dates]: /about/dates/
[Contacts]: /about/about-debconf/

<h3 class="fancy-heading"><span>Contacts</span></h3>

- For additional information and requests about registration: <registration@debconf.org>.
- Mailing lists:
* Participant discussions: <debconf-discuss@lists.debconf.org>, [subscriptions](http://lists.debconf.org/mailman/listinfo/debconf-discuss), [archives](http://lists.debconf.org/lurker/list/debconf-discuss.en.html)
* Organizer discussions: <debconf-team@lists.debconf.org>, [subscriptions](http://lists.debconf.org/mailman/listinfo/debconf-team), [archives](http://lists.debconf.org/lurker/list/debconf-team.en.html)
- IRC on `irc://irc.debian.org`
* General discussion: [`#debconf`][debconf-irc]
* DebConf team: [`#debconf-team`][debconf-team-irc]

[debconf-irc]: https://webchat.oftc.net/?channels=debconf
[debconf-team-irc]: https://webchat.oftc.net/?channels=debconf-team
