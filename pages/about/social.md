---
name: Social
---
DebConf 16 Social Events
========================

DebConf is a great event for Debian contributors to get to know each other better. We're planning some social events for DebConf 16 to help break the ice and provide a break from the screens and thinking really hard about things.

Conference Dinner
-----------------

The conference dinner is the main social event of DebConf, we expect to see most of the attendees there.

Birthday Party
--------------

Cake, interesting beverages, live music and good company. This is a light and fun event celebrating Debian's birthday. This year, the Debian project turns 23 years old!

Cheese and Wine Party
---------------------

The cheese and wine party has become a tradition at DebConf. It started in Helsinki during DebConf 5, in the so-called "French" room. Attendees bring cheese, wine and other delicacies from their region and during this session everyone gets to sample some of it. If you couldn't bring anything, that's fine too. Either way, consider helping out the cheese and wine party planners since there's a lot of work on the day (such as cutting cheese).

Poetry Night
------------

Show up and read some poetry! It can be something that you wrote or something that you like. Latest details will be on the [DebConf wiki](https://wiki.debconf.org/wiki/DebConf16/Poetry-night "").

Assassins Game
--------------

More info soon...

Key Signing
----------

Debian developers depend on building a cryptographic web of trust to communicate and transfer data securely. This trust is established in person by following a process called key signing which involves cross-checking keys and official ID documentation. It's also a great way to meet many people and put some faces to names and learn where they're from.
