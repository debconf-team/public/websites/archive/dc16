---
name: Visiting South Africa
---
<h1 class="fancy-heading"><span>Visiting South Africa</span></h1>

Requirements for all visitors to South Africa:

* A valid and acceptable passport or travel document for your intended stay.
* At least one blank page (two to be safe) in your passport for endorsements **important**
* A valid visa, if required
* Sufficient funds to pay for your day-to-day expenses during your stay
* A return or onward ticket
* Yellow fever certificates if your journey starts or entails passing through the yellow fever belt of Africa or South America.

<h2 class="fancy-heading"><span>Countries requiring visas</span></h2>
[Many nationalities][exempt-countries] are exempt from requiring a visa to enter South Africa, citizens of these countries may visit South Africa for up to 90 days (or in some cases 30 days) without a visa.
This includes most of Western Europe, USA, and Canada.

[exempt-countries]: http://www.dha.gov.za/index.php/immigration-services/exempt-countries 

<h2 class="fancy-heading"><span>Visa Application process</span></h2>

Other attendees need to [apply for a visa][].
Visa applications don't need to be made in-person — there is no visa interview — they can be done by courier.
[Some countries][visa fees] citizens are charged fees for a visa, for others the visa is free of charge.

[apply for a visa]: http://www.dha.gov.za/index.php/immigration-services/apply-for-a-south-african-visa
[visa fees]: http://www.southafrica-newyork.net/homeaffairs/visafees.htm

Conference attendees should apply for an ordinary tourist visitor visa ([Form BI-84][bi84]). If you need an invitation letter, please send your request to [visa@debconf.org](mailto:visa@debconf.org "Visa email") and make sure to include your full name, passport number and expiration date as well as your physical address.

[bi84]: http://www.southafrica-newyork.net/homeaffairs/PDF/Visitor's%20Visa%20Application%20Form%20(DHA-84)%20(Form%2011)%20(June%2019,%202014).pdf

* [South African missions abroad](http://www.dfa.gov.za/foreign/sa_abroad/index.htm).
* [Mission web sites](http://www.dfa.gov.za/webmissions/) (for missions that have them).

<h2 class="fancy-heading"><span>Bringing children to South Africa</span></h2>

South Africa has [strict requirements]
[travelling-with-children] for children travelling into or out of the country. Contact the [visa team] if you need any help navigating these.

[travelling-with-children]: http://www.dha.gov.za/index.php/civic-services/traveling-with-children
[visa team]: mailto:visa@debconf.org
