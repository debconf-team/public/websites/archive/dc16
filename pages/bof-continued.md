---
name: (...BoF continued)
---
All BoFs are 45 minutes long. If they're scheduled in a 20 minute slot, they actually run through two 20 minute slots together. (A quirk of the scheduling system prevents BoFs from being scheduled across two 20 minute session slots.)
