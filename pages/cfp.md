---
name: Call for Proposals
---
# DebConf16: Call for Proposals

The DebConf Content team is pleased to announce the Call for Proposals for the DebConf16 conference, to be held in **Cape Town, South Africa** from 2 through 9 July 2016.

## Submitting an Event

[Submit a talk](/talks/new/) and describe your proposal. Please note, events are not limited to traditional presentations or informal sessions (BoFs). We welcome submissions of tutorials, performances, art installations, debates, or any other format of event that you think would be beneficial to the Debian community.

Please include a short title, suitable for a compact schedule, and an engaging description of the event. You should use the field "_Notes_" to provide us information such as additional speakers, scheduling restrictions, or any special requirements we should consider for your event.

Regular sessions may either be 20 or 45 minutes long (including time for questions), other kinds of sessions (like workshops) could have different durations. Please choose the most suitable duration for your event and explain any special requests.

[your profile page]: /accounts/profile/

## Timeline

The first batch of accepted proposals will be announced in **April**. If you depend on having your proposal accepted in order to attend the conference, please submit it as soon as possible so that it can be considered during this first evaluation period.

The CFP has been extended by two weeks. All proposals must be submitted before **Sunday 15 May 2016** to be evaluated for the official schedule.

## Topics and Tracks

Though we invite proposals on any Debian or FLOSS related subject, we have some broad topics on which we encourage people to submit proposals, including:

- Debian Packaging, Policy, and Infrastructure
- Security, Safety, and Hacking
- Debian System Administration, Automation and Orchestration
- Containers and Cloud Computing with Debian
- Debian Success Stories
- Debian in the Social, Ethical, Legal, and Political Context
- Blends, Subprojects, Derivatives, and Projects using Debian
- Embedded Debian and Hardware-Level Systems

## Video Coverage

Providing video of sessions amplifies DebConf achievements and is one of the [conference goals]. Unless speakers opt-out, official events will be streamed live over the Internet to promote remote participation. Recordings will be published later under the [DebConf license], as well as presentation slides and papers whenever available.

[conference goals]: http://debconf.org/goals.shtml
[DebConf license]: http://meetings-archive.debian.net/pub/debian-meetings/LICENSE

## Contact and Thanks to Sponsors

DebConf would not be possible without the generous support of all our sponsors, especially our platinum sponsor [HPE]. DebConf16 is still accepting sponsors; if you are interested, please [get in touch]!

You are welcome to contact the Content Team with any concerns about your event, or with any ideas or questions about DebConf events in general. You can reach us at [content@debconf.org](mailto:content@debconf.org).

We hope to see you all in Cape Town!

[HPE]: http://www.hpe.com/engage/opensource
[get in touch]: /get-involved
