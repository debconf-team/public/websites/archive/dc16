---
name: Day Trip
---
<h1 class="fancy-heading"><span>Day Trip</span></h1>

The daytrip will be on Wednesday, 6 July. Cape Town has so much to offer, from cultural and scenic tours, safaris and animal viewing, and of course a plethora of wine farms and breweries.

Details on Day Trip options: <a href="https://wiki.debconf.org/wiki/DebConf16/Daytrip">https://wiki.debconf.org/wiki/DebConf16/Daytrip</a>

Sign-up at: <a href="https://wiki.debconf.org/wiki/DebConf16/DayTrip/Signup">https://wiki.debconf.org/wiki/DebConf16/DayTrip/Signup</a>
