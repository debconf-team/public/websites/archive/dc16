---
name: DebCamp Sprints
---
<h1 class="fancy-heading"><span>DebCamp Sprints</span></h1>

The DebCamp16 sprints are between Thursday, 23 June and Friday, 1 July 2016 on the UCT campus. They are located in the Snape building, rooms 2B and 2C. Further overflow space for sprints or general hacking is available in the Menzies building room 12 and Blue Lab, and in the common rooms of the Fuller building (the main dorm for the conference). See [the map](/about/debconf/) for the locations of the rooms. If you can't find where your team is sprinting, check in Snape 2A at the Front Desk, we'll keep a sprint signup pad there for ad-hoc lists of which sprints are where/when.

If your team is interested in organising a sprint, you will need to decide the: 

* Topic 
* Goals and tasks to be accomplished 
* The expected duration (one day? three days?) 
* and a sign-up section or any other information that might be useful 

The content team will maintain a sprints schedule on the DebConf16 website and 
link back to your sprint information on the Debian wiki. 

To register your sprint, you can do so by using the template on 
[https://wiki.debian.org/Sprints](https://wiki.debian.org/Sprints)

For some inspiration, there's also a HowTo: 
[https://wiki.debian.org/Sprints/HowTo](https://wiki.debian.org/Sprints/HowTo)

When you have a page describing your sprint, be sure to announce it to your 
team's mailing list, IRC, etc. 

If you need help in the coordination of space, dates and participants contact us at 
content@debconf.org.

<h2 class="fancy-heading"><span>Cocktail Evening, June 30th</span></h2>

On Thursday, June 30th, join us for a Cocktail Evening at the UCT Pub on campus, celebrating the end of DebCamp. Finger foods and 2 cocktails per person are complimentary.
