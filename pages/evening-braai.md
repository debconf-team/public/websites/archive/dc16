---
name: Evening Braai (UCT Pub)
---
<h1 class="fancy-heading">Evening Braai at the UCT Pub</h1>

Special dinner on the evening after the Day Trip, join us starting 19:00 at the UCT campus Pub for a traditional local Braai (barbeque).

Tickets to the Braai are available for purchase for R250 at the Front Desk. The Braai is included in the food package if you self-paid food for the
complete DebConf or are sponsored regarding food. In this case you do
not need to pay extra. Just show up at the Braai after your Day Trip.
