---
name: Get Involved
---
<h1 class="fancy-title"><span>How to help</span></h1>

Each year DebConf is made possible by the efforts of our volunteers and the support of our sponsors.

We invite you to help ensure that DebConf16 will be a success.

<h2 class="fancy-heading"><span>Volunteering</span></h2>

DebConf is organised by volunteers, and its quality strongly depends on this team's work, so all help is appreciated.

To volunteer, we have a new volunteer framework set up. See the [volunteers page](/volunteers/ "Volunteering").

### Want to Get Involved?

We still have unassigned work available, your contributions are welcome!

- Sign up to our mailing list at [lists.debconf.org] and introduce yourself.
- Find work in progress on our [wiki] and [git] repository.
- We have an IRC channel available on [#debconf-team] on the OFTC network.

## Donations

Without money we can not organise a DebConf.
Donating is a great way to help us.

Donations for DebConf16 are accepted through Debian's fiscal sponsor [SPI].

### SPI - USA

[SPI] can be paid via credit card using [Click&Pledge].
Click&Pledge can be used to make payments worldwide in USD via VISA, MasterCard or Discover.
If the name on the card does not match the attendee name, or you are paying for multiple attendees, please email <registration@debconf.org>.

<a href="https://co.clickandpledge.com/advanced/default.aspx?wid=41803" class="btn btn-primary">Donate!</a>

The SPI Treasurer (<treasurer@rt.spi-inc.org>) can be contacted to arrange other payment methods.

## Sponsoring
Would your organisation consider becoming one of our generous sponsors?
You can make a valuable contribution to DebConf while also seeing benefits come back to your organisation. Become a DebConf sponsor!

As volunteer based organisation, we highly depend on our sponsors to provide a financial backbone for the conference.
Each year we make brochures with various sponsorship information. If you're interested in sponsoring DebConf16 or at least curious please read our [DebConf16 sponsoring brochure] or contact us at <sponsors@debconf.org>.

[lists.debconf.org]: http://lists.debconf.org/mailman/listinfo/debconf-team
[wiki]: https://wiki.debconf.org/wiki/DebConf16
[git]: https://alioth.debian.org/projects/debconf-data/
[#debconf-team]: irc://irc.oftc.net/#debconf-team
[SPI]: http://www.spi-inc.org/
[Click&Pledge]: https://co.clickandpledge.com/advanced/default.aspx?wid=41803
[DebConf16 sponsoring brochure]: http://media.debconf.org/dc16/fundraising/debconf16_sponsorship_brochure.pdf
