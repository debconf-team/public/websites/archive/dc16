---
name: Group Photo during Tea/Coffee Break
---
<h1 class="fancy-heading"><span>Group Photo</span></h1>

All attendees, please join us on the steps above Fuller Hall for the group conference photo at 10:45am on Thursday, July 7th. If the weather is poor, the photo may be postponed from Thursday to Friday or Saturday, updates will be posted in the daily announcement emails.
