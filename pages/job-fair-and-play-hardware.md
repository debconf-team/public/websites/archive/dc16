---
name: Job Fair & Play Hardware
---
<h1 class="fancy-heading"><span>Job Fair</span></h1>

DebConf attracts some of the smartest Free Software hackers from around the globe, many of whom are always on the lookout for new challenges. We provide our sponsors with the chance to reach out to Debian contributors, derivative developers, upstream authors and other community members. The Job Fair runs 14:00 to 17:00 on Saturday. The job wall will be available throughout DebConf.

<h1 class="fancy-heading"><span>Play Hardware</span></h1>

The aim of this initiative during the DebConf16 Open Weekend is to showcase Debian running on various hardware doing different things, even unusual things. Play Hardware is at the same time and in the same location as the Job Fair on Saturday.

There is a current trend towards developing open hardware, and embedded systems. We are hoping to have people share their projects, their products and their passions, in any way they choose. Anyone is invited to join: companies, research groups, hobbyists, beginners, anyone! Please email indiebio@debconf.org to arrange a session, or just pitch up on Saturday morning.

More info: [https://wiki.debconf.org/wiki/DebConf16/DebianDay_Hardware](https://wiki.debconf.org/wiki/DebConf16/DebianDay_Hardware)

Highlights include a 3D printer from Aleph Objects.
