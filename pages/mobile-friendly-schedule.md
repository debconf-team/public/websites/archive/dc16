---
name: Mobile-friendly Schedule
---
<h1 class="fancy-heading"><span>Mobile-friendly Schedule</span></h1>

The Schedule is available through an XML feed. You can use _ConfClerk_ in Debian to consume this, or _Giggity_ on Android.

![Schedule QR Code](/static/img/dc16-schedule-qr-code.png "DebConf16 Schedule")

XML Feed: [https://debconf16.debconf.org/schedule/pentabarf.xml](https://debconf16.debconf.org/schedule/pentabarf.xml "https://debconf16.debconf.org/schedule/pentabarf.xml")

<h2 class="fancy-heading"><span>Download Giggity</span></h2>

- Home page: [https://gaa.st/giggity](https://gaa.st/giggity "https://gaa.st/giggity")
- F-Droid: [https://f-droid.org/repository/browse/?fdid=net.gaast.giggity](https://f-droid.org/repository/browse/?fdid=net.gaast.giggity "https://f-droid.org/repository/browse/?fdid=net.gaast.giggity")
- Google Play Store: [https://play.google.com/store/apps/details?id=net.gaast.giggity
](https://play.google.com/store/apps/details?id=net.gaast.giggity "https://play.google.com/store/apps/details?id=net.gaast.giggity")

<h2 class="fancy-heading"><span>Download ConfClerk</span></h2>

	# apt install confclerk
