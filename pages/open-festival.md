---
name: The Open Festival
---
<h1 class="fancy-heading"><span>The Open Festival</span></h1>

The weekend of 2 and 3 July is targeted at the general public where events of interest to a wider audience are offered, ranging from topics specific to Debian to a wider appreciation of the open and maker movements (and not just IT-related).

The Open Weekend is a great possibility for interested users to meet our community, for us to widen our community, and for our sponsors to increase their visibility.

<h2 class="fancy-heading"><span>Schedule</span></h2>

Here's what will be taking place over the weekend.

- Workshops 
- Lightning talks: Tell us what you are working on! 
- Job Fair on Saturday, the job wall will be available throughout DebConf 
- Information Art Exhibition / Posters 
- Linux Installation Assistance - CLUG (Cape Linux Users Group) 
- Orienteering ice-breaker: learn the campus, end with a well-earned beer

See the [full schedule](/schedule) for more details.

<h2 class="fancy-heading"><span>Activities</span></h2>

<h3>Hardware Exhibition & Hacking</h3>

There is a current trend towards developing open hardware, and embedded systems. We are hoping to have people share their projects, their products and their passions, in any way they choose. More info: [https://wiki.debconf.org/wiki/DebConf16/DebianDay_Hardware](https://wiki.debconf.org/wiki/DebConf16/DebianDay_Hardware)

<h3>Poster Art Exhibition</h3>

There will be a poster exhibition - think information art exhibition rather than 'conference'. You are encouraged to share resources with the community, that can also spread your word during the more technical component of the conference in the following week. If you have data visualisation work, please consider sharing it with us! More info: [https://wiki.debconf.org/action/edit/DebConf16/DebianDay/Posters](https://wiki.debconf.org/action/edit/DebConf16/DebianDay/Posters)

<h3>Talks</h3>

During the open weekend there will be talks by local and international guests. Please invite or suggest people via [https://debconf16.debconf.org/cfp/](https://debconf16.debconf.org/cfp/) or by emailing content@debconf.org. While the scheduled talks for the technical part of the conference has been concluded (see [Talks](https://debconf16.debconf.org/talks/) for highlights) the open weekend is designed to be like a festival - just pitch up and we'll make a plan! (But plan ahead and be awesome)

<h3>Notes</h3>

- Cost of attendance is FREE.
- The Open Weekend is generally very informal and relaxed, with specific events scheduled as requested by participants.
- Venue: Menzies level 3, University of Cape Town Upper Campus, Rondebosch.
  - [Map](http://www.icts.uct.ac.za/modules.php?name=News&file=article&sid=5990 "Map") 
- Sponsoring: DebConf16 is a volunteer run conference, and most of our funds go towards funding food, travel and accommodation of Debian contributors, so we need any help we can get. While sponsor contributions are always appreciated, the Open Weekend will be FREE to attend, and exhibit at: Our aim is to build the local community.
- The Job Fair is only available to Silver, Gold and Platinum sponsors. While the official time is from 10:00 to 14:00, these sponsors are welcome to exhibit until the end of the weekend. The job wall will be available throughout DebConf. 

<h2 class="fancy-heading"><span>FAQ's on getting involved</span></h2>

- Industry is welcome to exhibit public-minded offerings.
- Government and community groups are welcome to exhibit their activities.
- Space will be provided for impromptu hack and tinker sessions, please contact us beforehand if possible to assist in planning, but you are also welcome to just show up on the day.
- Contact indiebio@debconf.org for more information or to get involved. Please put "Open Festival" in your subject line. 

There will also be some stickers for sale, more info and to add yours: [https://wiki.debconf.org/wiki/DebConf16/Merch](https://wiki.debconf.org/wiki/DebConf16/Merch)
