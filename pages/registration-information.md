---
name: Registration Information
---
<h1 class="fancy-heading"><span>Registration Information</span></h1>

<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Information</h3>
  </div>
  <div class="panel-body">
    <p>Registration is open for DebConf16. The conference is taking place in Cape Town, South Africa from Saturday, 2 July to Saturday, 9 July 2016 at the University of Cape Town. Attendees must register by Sunday, 10 April 2016 to apply for sponsored food, accommodation, or travel. After this date, registrations will still be accepted in any of the basic, professional, and corporate categories, but requests for sponsorship will no longer be accepted, and accommodation at campus will no longer be guaranteed. Register as soon as possible, to be sure of being accommodated!
</p>
    <p>The conference will be held at the upper campus of the University of Cape Town in Newlands/Rondebosch, Cape Town, South Africa. On-campus accommodations will be available to both sponsored and non-sponsored attendees at the on-campus Fuller (and if we grow >200 people, Smuts) residencies. Meals will be served in the cafeteria of Fuller Hall, at UCT Club and possibly at other locations within the upper campus of UCT. Additional external (non-DebConf) nearby accommodation are listed in the wiki.</p>
  </div>
</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Registration fees</h3>
  </div>
  <div class="panel-body">
    <p>As always, basic registration for DebConf is free of charge for attendees. If you are attending the conference in a professional capacity, or as a representative of your company, we ask that you consider registering in one of our paid categories, to help cover the costs of putting on the conference:</p>
    <p><strong>Professional registration:</strong> with a rate of 200 USD for the week, this covers the costs associated with your individual attendance at the conference.</p>
    <p><strong>Corporate registration:</strong> with a registration fee of 500 USD, this category is intended for those participating in DebConf as representatives of companies and helps to subsidise the cost of the conference for others.</p>
    <p>As in previous three DebConfs, these registration fees do not include food or accommodation.</p>
    <p>We encourage all attendees to help make DebConf a success by selecting the registration category appropriate for their situation.</p>
  </div>
</div>
</div>

</div>
<div class="row">

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">How to Register</h3>
  </div>
  <div class="panel-body">
    <p>If you want to attend DebConf16, please fill out the <strong>registration form</strong>, linked from your <a href="/accounts/profile/">profile</a>.</p>
    <p> As in recent years, the conference website is integrated with the Debian SSO system. However, Debian SSO has changed, the new system is based on SSL Client certificates. If you are a Debian developer and have not previously used the Debian SSO system, you will need to configure an SSO password on <a href="https://db.debian.org/">db.debian.org</a>. If you are not a Debian developer, you will have an opportunity to <a href="https://alioth.debian.org/account/register.php">create an alioth account</a> as part of the registration process. For more information see <a href="https://wiki.debian.org/DebianSingleSignOn">Debian single sign-on</a> page. There is also the possibility to sign up without the SSO, but we highly recommend the the Debian community use SSO to sign up.</p>
    <p>For any further questions related to registration please join <a href="https://webchat.oftc.net/?channels=debconf-team"><code>#debconf-team</code></a> on OFTC or mail <a href="mailto:registration@debconf.org">registration@debconf.org</a>.</p>
    <p class="text-center"><a href="/accounts/profile/" class="btn btn-primary btn-lg">Register Now</a></p>
  </div>
</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Personal Information</h3>
  </div>
  <div class="panel-body">
<p>Due to the vagaries of the conference management system, your details have to be edited in a few different places. These are linked to from the right hand side of your profile page, and your registration form.</p>
<p>To edit your name and e-mail address, follow the "Edit User" link. Your name (combining <strong>First Name</strong> (if set) and <strong>Last Name</strong>) is used as first line in the badge, as the speaker/moderator name on submitted events. Additionally we will use such name to contact you, and for our lists (e.g. day trip options).</p>
<p>Your email address is the preferred address that DebConf registration team will use to contact you.</p>
<p>The <strong>Nickname on my badge</strong> and <strong>My group, team or organisation</strong> registration fields are printed as second and third lines on your badge. To simplify identification, we recommend, to the Debian members and contributors, to include also the Debian (or Alioth) login name and IRC (if different). But you are free to choose what you like (assuming it is not offensive, see the Codes of Conduct).</p>
<p>We recommend that you provide a contact phone number, this time behind the "Edit Profile" link. This should be the number for your mobile phone that you carry at DebConf (if you do), and it will used only in emergency or if we can't find you and are worried about leaving you behind (e.g. during the day trip).</p>
<p>The <strong>Emergency contact</strong> should be the phone number and/or e-mail address of a friend or family member that we should contact, if something were to happen to you. We hope to never need this.</p>
  </div>
</div>
</div>

</div>
