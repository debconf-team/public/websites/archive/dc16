---
name: Site Credits
---
<h1 class="fancy-heading"><span>Site Credits</span></h1>

This site is built on the [wafer][] conference management system. Wafer uses the [Django][] framework, and is licensed under the [ISC license][].

[wafer]: https://github.com/ctpug/wafer
[isc license]: https://github.com/CTPUG/wafer/blob/master/LICENSE
[django]: http://djangoproject.com/

The [source] is also licensed under the ISC license. With the exception of:

* Sponsor logos
* Images obtained from third parties - [see list][image-authors]
* Fonts - [see list][font-authors]
* Bundled libraries - [see list][bower]

[source]: https://anonscm.debian.org/cgit/debconf-data/dc16.dc.o.git/
[image-authors]: https://anonscm.debian.org/cgit/debconf-data/dc16.dc.o.git/tree/assets/images/AUTHORS
[font-authors]: https://anonscm.debian.org/cgit/debconf-data/dc16.dc.o.git/tree/assets/fonts/AUTHORS
[bower]: https://anonscm.debian.org/cgit/debconf-data/dc16.dc.o.git/tree/bower.json
