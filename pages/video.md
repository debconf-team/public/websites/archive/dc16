---
name: Video
---
<h2 class="fancy-heading"><span>DebConf Video Archives</span></h2>

DebConf talks and many DebConf sessions are recorded on video. You can browse [previous DebConf videos on the Debian meetings archive](http://meetings-archive.debian.net/pub/debian-meetings/ "").

<h2 class="fancy-heading"><span>Live Streaming</span></h2>

Talks and sessions are being streamed live allowing remote participants to be involved via IRC.

<a href="/video/live-streaming/" class="btn btn-primary"><i class="fa fa-fw fa-video-camera"></i> Live Streaming</a>

<h2 class="fancy-heading"><span>Get Involved</span></h2>

Attending DebConf16? The video team has a large range of different tasks and there might just be something that's fun and interesting to you.

* Video team wiki page: [https://wiki.debconf.org/wiki/DebConf16/Videoteam](https://wiki.debconf.org/wiki/DebConf16/Videoteam "")
* IRC: #debconf-video on oftc
* Mailing list: [http://lists.debconf.org/mailman/listinfo/debconf-video](http://lists.debconf.org/mailman/listinfo/debconf-video "")
