---
name: Live Streaming
---
<h1 class="fancy-heading"><span>Live Streaming</span></h1>

We are live streaming the main talk rooms, as well as the primary BoF room. You can watch the live streams from the buttons below:

<div class="text-center">
  <a href="/schedule/venue/1/" class="btn btn-primary"><i class="fa fa-video-camera" aria-hidden="true"></i> Menzies 9 (talk room)</a>
  <a href="/schedule/venue/2/" class="btn btn-primary"><i class="fa fa-video-camera" aria-hidden="true"></i> Menzies 10 (talk room)</a>
  <a href="/schedule/venue/3/" class="btn btn-primary"><i class="fa fa-video-camera" aria-hidden="true"></i> Menzies 12 (BoF room)</a>
</div>

## IRC Channels

All rooms also have IRC channels. The channels for the main rooms are #debconf16-menzies-9, #debconf16-menzies-10 and #debconf16-menzies-12 on OFTC.
