import os

from django.conf import settings
from django.conf.urls import include, patterns, url
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
import volunteers.views

urlpatterns = patterns(
    '',
    url(r'', include('dc16.urls')),
    url(r'^registration/?$', RedirectView.as_view(
        url='/about/registration/', permanent=True)),
    url(r'^volunteers/$', volunteers.views.promo, name='promo'),
    url(r'^volunteers/faq/$', volunteers.views.faq, name='faq'),
    url(r'^volunteers/signup$', volunteers.views.signup, name='signup'),
    url(r'^volunteer/(?P<username>(?!signout|signup|signin)[\.\w-]+)/$', volunteers.views.profile_detail, name='profile_detail'),
    url(r'^volunteer/(?P<username>[\.\w-]+)/edit/$', volunteers.views.profile_edit, name='userena_profile_edit'),
    url(r'^volunteers/page/(?P<page>[0-9]+)/$', volunteers.views.ProfileListView.as_view(), name='userena_profile_list_paginated'),
    url(r'^volunteers/list$', volunteers.views.ProfileListView.as_view(), name='userena_profile_list'),
    url(r'^volunteers/tasks/(?P<username>[\.\w-]+)', volunteers.views.task_list_detailed, name='task_list_detailed'),
    url(r'^volunteers/task/(?P<task_id>\d+)/$', volunteers.views.task_detailed, name='task_detailed'),
    url(r'^volunteers/talk/(?P<talk_id>\d+)/$', volunteers.views.talk_detailed, name='talk_detailed'),
    url(r'^volunteers/tasks/', volunteers.views.task_list, name='task_list'),
    url(r'^volunteers/schedule/today/$', volunteers.views.grid_today, name='grid_today'),
    url(r'^volunteers/schedule/(?P<date>[\d-]+)/$', volunteers.views.task_grid, name='task_grid'),
    url(r'^volunteers/talks/$', volunteers.views.talk_list, name='talk_list'),
    url(r'^volunteers/category_schedule/$', volunteers.views.category_schedule_list, name='category_schedule_list'),
    url(r'^volunteers/task_schedule/(?P<template_id>\d+)/$', volunteers.views.task_schedule, name='task_schedule'),
    url(r'^volunteers/task_schedule_csv/(?P<template_id>\d+)/$', volunteers.views.task_schedule_csv, name='task_schedule_csv'),
)

if settings.DEBUG:
    urlpatterns += static(
        'build',
        document_root=os.path.join(os.path.dirname(__file__), 'build'))

urlpatterns.append(url(r'', include('wafer.urls')))
